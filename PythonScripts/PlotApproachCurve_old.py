# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 14:45:10 2023

@author: Luzifer
"""

import matplotlib.pyplot as plt
import numpy as np
import sys
from matplotlib.offsetbox import AnchoredText

def main():
    path = "C:\MaskAligner\Data\ApproachCurve16_08_23.csv"
    save = ""
    
    #Simple command line parsing
    if("--file" in sys.argv or "-f" in sys.argv):
        for i in range(len(sys.argv)):
            if(sys.argv[i] == "--file" or sys.argv[i] == "--f"):
                try:
                    path = sys.argv[i+1]
                except:
                    print("incorrect file path.")
                    return
                    
    if("--save" in sys.argv or "-s" in sys.argv):
        for i in range(len(sys.argv)):
            if(sys.argv[i] == "--save" or sys.argv[i] == "--s"):
                try:
                    save = sys.argv[i+1]
                except:
                    print("incorrect save file name.")
                    return

    data = np.genfromtxt(path, delimiter=",")
    
    x = data.T[0]
    c1 = data.T[1]
    c2 = data.T[2]
    c3 = data.T[3]
    
    step_s = data.T[4][0]
    step_s_err = data.T[5][0]
    
    
    c_diff_1 = np.ones(len(c1) - 1)
    c_diff_2 = np.ones(len(c2) - 1)
    c_diff_3 = np.ones(len(c2) - 1)
    
    for i in range(len(c1)-1):
        c_diff_1[i] = c1[i+1] - c1[i]
        c_diff_2[i] = c2[i+1] - c2[i]
        c_diff_3[i] = c3[i+1] - c3[i]
    
    del data

    lw = 0.75
    ms = 2
    
    fig, ax = plt.subplots(2, 3)
    fig.set_figheight(9)
    fig.set_figwidth(16)
    
    ax[0,0].plot(x, c1, color="tab:red", marker="x", markersize=ms, linestyle="--", linewidth=lw)
    ax[0,0].set_title("Capacitance 1")
    ax[0,0].set_xlabel("Step [" + str(step_s) + "±" + str(step_s_err) + " nm]")
    ax[0,0].set_ylabel("Capacitance [pF]")
    
    ax[0,1].plot(x, c2, color="tab:green", marker="x", markersize=ms, linestyle="--", linewidth=lw)
    ax[0,1].set_title("Capacitance 2")
    ax[0,1].set_xlabel("Step [" + str(step_s) + "±" + str(step_s_err) + " nm]")
    ax[0,1].set_ylabel("Capacitance [pF]")
    
    ax[0,2].plot(x, c3, color="tab:blue", marker="x", markersize=ms, linestyle="--", linewidth=lw)
    ax[0,2].set_title("Capacitance 3")
    ax[0,2].set_xlabel("Step [" + str(step_s) + "±" + str(step_s_err) + " nm]")
    ax[0,2].set_ylabel("Capacitance [pF]")
    
    ax[1,0].plot(x[1:], c_diff_1, color="tab:red", marker="x", markersize=ms, linestyle="--", linewidth=lw)
    ax[1,0].set_title("Diff Capacitance 1")
    ax[1,0].set_xlabel("Step [" + str(step_s) + "±" + str(step_s_err) + " nm]")
    ax[1,0].set_ylabel("Capacitance [pF]")
    #ax[1,0].set_ylim(-0.0025, 0.02)
    
    ax[1,1].plot(x[1:], c_diff_2, color="tab:green", marker="x", markersize=ms, linestyle="--", linewidth=lw)
    ax[1,1].set_title("Diff Capacitance 2")
    ax[1,1].set_xlabel("Step [" + str(step_s) + "±" + str(step_s_err) + " nm]")
    ax[1,1].set_ylabel("Capacitance [pF]")
    #ax[1,1].set_ylim(-0.0015, 0.008)
    
    ax[1,2].plot(x[1:], c_diff_3, color="tab:blue", marker="x", markersize=ms, linestyle="--", linewidth=lw)
    ax[1,2].set_title("Diff Capacitance 3")
    ax[1,2].set_xlabel("Step [" + str(step_s) + "±" + str(step_s_err) + " nm]")
    ax[1,2].set_ylabel("Capacitance [pF]")
    #ax[1,2].set_ylim(-0.002, 0.005)
    
    fig.tight_layout()
    plt.show()
    
    #Determine Savefile name if not already specified
    if(save == ""):
        path_parts = []
        if("\\" in path):
            path_parts = path.split("\\")
        elif ("/" in path):
            path_parts = path.split("/")
            
        save = "Plots/ApproachCurves/" + path_parts[-1].replace(".csv", "")
        
    fig.savefig(save + ".pdf")
    fig.savefig(save + ".png", dpi=300)

if __name__ == "__main__":
    main()