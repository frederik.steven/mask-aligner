import numpy as np
import matplotlib.pyplot as plt
import sys

def plottingSingle(rot, zp, zm, err_t = 1, name="z1"):
    plt.errorbar(rot, zp, err_t, label=name + " down")
    plt.errorbar(rot, zm, err_t, label=name + " up")
    
    plt.xlabel("screw rotations")
    plt.ylabel("t [s]")
    plt.legend()
    plt.show()

def plottingAll(rot, z1p, z1m, z2p, z2m, z3p, z3m, err_t = np.sqrt(2)):
    plt.errorbar(rot, z1p, err_t, label="z1 down")
    plt.errorbar(rot, z1m, err_t, label="z1 up")
    plt.errorbar(rot, z2p, err_t, label="z2 down")
    plt.errorbar(rot, z2m, err_t, label="z2 up")
    plt.errorbar(rot, z3p, err_t, label="z3 down")
    plt.errorbar(rot, z3m, err_t, label="z3 up")
    
    plt.xlabel("screw rotations")
    plt.ylabel("t [s]")
    plt.legend()
    plt.show()
    
def plottingRatio(rot, z1, z2, z3, err_t = 1):
    plt.errorbar(rot, z1, err_t, label="z1 up/down")
    plt.errorbar(rot, z2, err_t, label="z2 up/down")
    plt.errorbar(rot, z3, err_t, label="z3 up/down")
    
    plt.xlabel("screw rotations")
    plt.ylabel("t [s]")
    plt.legend()
    plt.show()

def main():
    if("--no_plots" in sys.argv):
        plots = False

    plt.style.use('tableau-colorblind10')

    file = ""
    if("--file" in sys.argv or "-f" in sys.argv):
        for i in range(len(sys.argv)):
            if("--file" in sys.argv[i] or "-f" == sys.argv[i]):
                try:
                    file = sys.argv[i+1]
                except:
                    print("No file or incorrect file specification.")
                    
    data = np.genfromtxt(file, delimiter=",", skip_header=1)
    data = data.T
    rot = data[0]
    z1p = data[1]
    z1m = data[2]
    z2p = data[3]
    z2m = data[4]
    z3p = data[5]
    z3m = data[6]
    
    err_t = 1.5
    
    plottingSingle(rot, z1p, z1m, err_t)
    plottingSingle(rot, z2p, z2m, err_t, "z2")
    plottingSingle(rot, z3p, z3m, err_t, "z3")

    plottingAll(rot, z1p, z1m, z2p, z2m, z3p, z3m)
    
    plottingRatio(rot, z1p/z1m, z2p/z2m, z3p/z3m)
    
    print(z1p/z1m)
    print(z2p/z2m)
    print(z3p/z3m)

if __name__ == "__main__":
    main()