import numpy as np
import matplotlib.pyplot as plt

def main():
    path = "C:/MaskAligner/Data/RHKDAta.csv"
    data = np.genfromtxt(path, skip_header=1, delimiter=",")
    data = data.T
    U_2ms = data[0]
    U_1ms = data[3]
    Down_2ms = data[1]
    Down_1ms = data[4]
    Up_2ms = data[2]
    Up_1ms = data[5]
    
    err = 1
    
    plt.errorbar(U_2ms, Down_2ms, Down_2ms * 0.1, label="+ 2ms", capsize=3, color="tab:blue")
    plt.errorbar(U_2ms, Up_2ms, Up_2ms * 0.1, label="- 2ms", capsize=3, color="tab:red")
    plt.plot(U_2ms, Up_2ms /2, linestyle="dashed", color="tab:red")
    plt.plot(U_2ms, Down_2ms /2, linestyle="dashed", color="tab:blue")
    plt.errorbar(U_1ms, Down_1ms, Down_1ms * 0.1, label="+ 1ms", capsize=3, color="tab:cyan")
    plt.errorbar(U_1ms, Up_1ms, Up_1ms * 0.1, label="- 1ms", capsize=3, color="tab:orange")
    
    plt.xlabel("Voltage [V]")
    plt.ylabel("1/speed [s/mm]")
    plt.legend()
    plt.savefig("Plots/RHKDatadep.pdf")
    plt.savefig("Plots/RHKDatadep.png", dpi=300)
    plt.show()

if __name__ == "__main__":
    main()