import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from matplotlib.offsetbox import AnchoredText
import sys
## Performs linear fit for calibration

def FitAndPlot(xp, xm, z1p, z1m, z2p, z2m, z3p, z3m, zAllp, zAllm, Voltage, plots, raw):
    #Linewidth
    lw = 0.8
    
    #
    if(plots):
        if(raw):
            plt.title("Calibration Measurement " + str(Voltage))
            plt.plot(xp, z1p, marker=".", color="tab:blue", label="z1-", linewidth=lw)
            plt.plot(xp, z1m, marker=".", color="b", label="z1+", linewidth=lw)
            plt.plot(xp, z2p, marker=".", color="tab:red", label="z2-", linewidth=lw)
            plt.plot(xp, z2m, marker=".", color="chocolate", label="z2+", linewidth=lw)
            plt.plot(xp, z3p, marker=".", color="tab:green", label="z3-", linewidth=lw)
            plt.plot(xp, z3m, marker=".", color="darkolivegreen", label="z3+", linewidth=lw)
            plt.plot(xp, zAllp, marker=".", color="tab:purple", label="zAll-", linewidth=lw)
            plt.plot(xp, zAllm, marker=".", color="m", label="zAll+", linewidth=lw)
            plt.ylabel(r"Measured Movement [$\mu m$]")
            plt.xlabel("# of step")
            plt.legend()
            plt.grid()
            plt.savefig("Plots\Calibration\\" + str(Voltage) + "_All_Data.pdf")
            plt.show()
         
    # Fit 
    # Errors are 1/sqrt(12) * one pixel length in mikrometers
    error_z1 = 8.87 / np.sqrt(12)
    error_z2 = 9.62 / np.sqrt(12)
    error_z3 = 9.80 / np.sqrt(12)
    error_zAll = 9.00 / np.sqrt(12)
    
    popt_z1p, pcov_z1p = curve_fit(linear, xp, z1p, error_z1)
    popt_z1m, pcov_z1m = curve_fit(linear, xp, z1m, error_z1)
    popt_z2p, pcov_z2p = curve_fit(linear, xp, z2p, error_z2)
    popt_z2m, pcov_z2m = curve_fit(linear, xp, z2m, error_z2)
    popt_z3p, pcov_z3p = curve_fit(linear, xp, z3p, error_z3)
    popt_z3m, pcov_z3m = curve_fit(linear, xp, z3m, error_z3)
    popt_zAllp, pcov_zAllp = curve_fit(linear, xp, zAllp, error_zAll)
    popt_zAllm, pcov_zAllm = curve_fit(linear, xp, zAllm, error_zAll)
    
    # Chi squared Computation per degree of freedom 
    chi_z1p = np.sum(((z1p - linear(xp, popt_z1p[0]))**2 / error_z1**2)) / (len(z1p) - 1)
    chi_z1m = np.sum(((z1m - linear(xp, popt_z1m[0]))**2 / error_z1**2)) / (len(z1m) - 1)
    chi_z2p = np.sum(((z2p - linear(xp, popt_z2p[0]))**2 / error_z2**2)) / (len(z2p) - 1)
    chi_z2m = np.sum(((z2m - linear(xp, popt_z2m[0]))**2 / error_z2**2)) / (len(z2m) - 1)
    chi_z3p = np.sum(((z3p - linear(xp, popt_z3p[0]))**2 / error_z3**2)) / (len(z3p) - 1)
    chi_z3m = np.sum(((z3m - linear(xp, popt_z3m[0]))**2 / error_z3**2)) / (len(z3m) - 1)
    chi_zAllp = np.sum(((zAllp - linear(xp, popt_zAllp[0]))**2 / error_zAll**2)) / (len(zAllp) - 1)
    chi_zAllm = np.sum(((zAllm - linear(xp, popt_zAllm[0]))**2 / error_zAll**2)) / (len(zAllm) - 1)
    
    # Plots
    fig, ax = plt.subplots(2, 2)
    fig.set_figheight(9)
    fig.set_figwidth(16)

    # Z1 
    capsize = 2
    if(plots):
        ax[0,0].set_title("Z1 calibration fit " + str(Voltage))
        ax[0,0].set_xlabel("# of steps")
        ax[0,0].set_ylabel(r"Measured Movement [$\mu m$]")
        ax[0,0].errorbar(xp, z1p, error_z1, linestyle="none", marker=".", linewidth=lw, color="tab:blue", label="z1- data", capsize=capsize)
        ax[0,0].errorbar(xp, z1m, error_z1, linestyle="none", marker=".", linewidth=lw, color="b", label="z1+ data", capsize=capsize)
        ax[0,0].plot(xp, linear(xp, popt_z1p[0]), linewidth=lw, color="tab:blue", label="z1- fit")
        ax[0,0].plot(xp, linear(xp, popt_z1m[0]), linewidth=lw, color="b", label="z1+ fit")
    
        #ax = plt.gca()
        anchored_text = AnchoredText(r"$b_-$= {:.2f} $\pm$ {:.2f} $n m$/step, $\chi^2/ndof =${:.2f}".format(popt_z1p[0] * 1000, np.sqrt(pcov_z1p[0][0]) * 1000, chi_z1p) + "\n" + "$b_+$= {:.2f} $\pm$ {:.2f} $n m$/step, $\chi^2/ndof =${:.2f}".format(popt_z1m[0] * 1000, np.sqrt(pcov_z1m[0][0]) * 1000, chi_z1m), loc=4)
        ax[0,0].add_artist(anchored_text)
        
        ax[0,0].legend()
        plt.grid()
        #plt.savefig("Plots\Calibration\Z1_" + str(Voltage) + ".pdf")
        #plt.show()
    
    print("slope in nm/step z1-:", popt_z1p[0] * 1000, " +- ", np.sqrt(pcov_z1p[0][0]) * 1000, " chi^2 ", chi_z1p)
    print("slope in nm/step z1+:", popt_z1m[0] * 1000, " +- ", np.sqrt(pcov_z1m[0][0]) * 1000, " chi^2 ", chi_z1m)
    
    #Z2
    if(plots):
        ax[0,1].set_title("Z2 calibration fit " + str(Voltage))
        ax[0,1].set_xlabel("# of steps")
        ax[0,1].set_ylabel(r"Measured Movement [$\mu m$]")
        ax[0,1].errorbar(xp, z2p, error_z2, linestyle="none", marker=".", linewidth=lw, color="tab:red", label="z2- data", capsize=capsize)
        ax[0,1].errorbar(xp, z2m, error_z2, linestyle="none", marker=".", linewidth=lw, color="chocolate", label="z2+ data", capsize=capsize)
        ax[0,1].plot(xp, linear(xp, popt_z2p[0]), linewidth=lw, color="tab:red", label="z2- fit")
        ax[0,1].plot(xp, linear(xp, popt_z2m[0]), linewidth=lw, color="chocolate", label="z2+ fit")
    
        #ax = plt.gca()
        anchored_text = AnchoredText(r"$b_-$= {:.2f} $\pm$ {:.2f} $n m$/step, $\chi^2/ndof =${:.2f}".format(popt_z2p[0] * 1000, np.sqrt(pcov_z2p[0][0]) * 1000, chi_z2p) + "\n" + "$b_+$= {:.2f} $\pm$ {:.2f} $n m$/step, $\chi^2/ndof =${:.2f}".format(popt_z2m[0] * 1000, np.sqrt(pcov_z2m[0][0]) * 1000, chi_z2m), loc=4)
        ax[0,1].add_artist(anchored_text)
        
        ax[0,1].legend()
        plt.grid()
        #plt.savefig("Plots\Calibration\Z2_" + str(Voltage) + ".pdf")
        #plt.show()
    
    print("slope in nm/step z2-:", popt_z2p[0] * 1000, " +- ", np.sqrt(pcov_z2p[0][0]) * 1000, " chi^2 ", chi_z2p)
    print("slope in nm/step z2+:", popt_z2m[0] * 1000, " +- ", np.sqrt(pcov_z2m[0][0]) * 1000, " chi^2 ", chi_z2m)
    
    #Z3
    if(plots):
        ax[1,0].set_title("Z3 calibration fit " + str(Voltage))
        ax[1,0].set_xlabel("# of steps")
        ax[1,0].set_ylabel(r"Measured Movement [$\mu m$]")
        ax[1,0].errorbar(xp, z3p, error_z3, linestyle="none", marker=".", linewidth=lw, color="tab:green", label="z3- data", capsize=capsize)
        ax[1,0].errorbar(xp, z3m, error_z3, linestyle="none", marker=".", linewidth=lw, color="darkolivegreen", label="z3+ data", capsize=capsize)
        ax[1,0].plot(xp, linear(xp, popt_z3p[0]), linewidth=lw, color="tab:green", label="z3- fit")
        ax[1,0].plot(xp, linear(xp, popt_z3m[0]), linewidth=lw, color="darkolivegreen", label="z3+ fit")
    
        #ax = plt.gca()
        anchored_text = AnchoredText(r"$b_-$= {:.2f} $\pm$ {:.2f} $n m$/step, $\chi^2/ndof =${:.2f}".format(popt_z3p[0] * 1000, np.sqrt(pcov_z3p[0][0]) * 1000, chi_z3p) + "\n" + "$b_+$= {:.2f} $\pm$ {:.2f} $n m$/step, $\chi^2/ndof =${:.2f}".format(popt_z3m[0] * 1000, np.sqrt(pcov_z3m[0][0]) * 1000, chi_z3m), loc=4)
        ax[1,0].add_artist(anchored_text)
        
        ax[1,0].legend()
        plt.grid()
        #plt.savefig("Plots\Calibration\Z3_" + str(Voltage) + ".pdf")
        #plt.show()
    
    print("slope in nm/step z3-:", popt_z3p[0] * 1000, " +- ", np.sqrt(pcov_z3p[0][0]) * 1000, " chi^2 ", chi_z3p)
    print("slope in nm/step z3+:", popt_z3m[0] * 1000, " +- ", np.sqrt(pcov_z3m[0][0]) * 1000, " chi^2 ", chi_z3p)
    
    #ZALL
    if(plots):
        ax[1,1].set_title("ZAll calibration fit " + str(Voltage))
        ax[1,1].set_xlabel("# of steps")
        ax[1,1].set_ylabel(r"Measured Movement [$\mu m$]")
        ax[1,1].errorbar(xp, zAllp, error_zAll, linestyle="none", marker=".", linewidth=lw, color="tab:purple", label="zAll- data", capsize=capsize)
        ax[1,1].errorbar(xp, zAllm, error_zAll, linestyle="none", marker=".", linewidth=lw, color="m", label="zAll+ data", capsize=capsize)
        ax[1,1].plot(xp, linear(xp, popt_zAllp[0]), linewidth=lw, color="tab:purple", label="zAll- fit")
        ax[1,1].plot(xp, linear(xp, popt_zAllm[0]), linewidth=lw, color="m", label="zAll+ fit")
    
        #ax = plt.gca()
        anchored_text = AnchoredText(r"$b_-$= {:.2f} $\pm$ {:.2f} $n m$/step, $\chi^2/ndof =${:.2f}".format(popt_zAllp[0] * 1000, np.sqrt(pcov_zAllp[0][0]) * 1000, chi_zAllp) + "\n" + "$b_+$= {:.2f} $\pm$ {:.2f} $n m$/step, $\chi^2/ndof =${:.2f}".format(popt_zAllm[0] * 1000, np.sqrt(pcov_zAllm[0][0]) * 1000, chi_zAllm), loc=4)
        ax[1,1].add_artist(anchored_text)
        
        ax[1,1].legend()
        plt.grid()
        plt.savefig("Plots\Calibration\Z_" + str(Voltage) + ".pdf")
        plt.savefig("Plots\Calibration\Z_" + str(Voltage) + ".png", dpi=300)
        plt.show()
    
    print("slope in nm/step zAll-:", popt_zAllp[0] * 1000, " +- ", np.sqrt(pcov_zAllp[0][0]) * 1000, " chi^2 ", chi_zAllp)
    print("slope in nm/step zAll+:", popt_zAllm[0] * 1000, " +- ", np.sqrt(pcov_zAllm[0][0]) * 1000, " chi^2 ", chi_zAllp)

def linear(x, b):
    return b*x

def dataHandling(data, plots, raw):
    x = data[0]
    z1p = data[1]
    z1m = data[2]
    z2p = data[3]
    z2m = data[4]
    z3p = data[5]
    z3m = data[6]
    # Check if calibration performed for all otherwise use dummy data.
    if(len(data) > 7):
        zAllp = data[7]
        zAllm = data[8]
    else:
        zAllp = np.zeros(len(z1p))
        zAllm = np.zeros(len(z1m))
        
    FitAndPlot(x, x, z1p, z1m, z2p, z2m, z3p, z3m, zAllp, zAllm, 85, plots, raw)

def main():
    plots = True
    raw = False

    file = "C:\MaskAligner\Data\Calibration.csv"

    # very Simple commandline parser
    if("--no_plots" in sys.argv):
        plots = False
    
    if("--file" in sys.argv or "-f" in sys.argv):
        for i in range(len(sys.argv)):
            if("--file" in sys.argv[i] or "-f" == sys.argv[i]):
                try:
                    file = sys.argv[i+1]
                except:
                    print("No file or incorrect file specification.")
    
    if("--raw_plot" in sys.argv):
        raw = True

    data = np.genfromtxt(file, skip_header=0, delimiter=",")
    data = data.T
    
    dataHandling(data, plots, raw)

if __name__ == "__main__":
    main()