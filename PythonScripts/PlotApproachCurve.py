# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 14:45:10 2023

@author: Luzifer
"""

import matplotlib.pyplot as plt
import numpy as np
import sys
from matplotlib.offsetbox import AnchoredText
from scipy.optimize import curve_fit

def func(x, shift, stretch):
    return shift + x * stretch

def linear(x, a, b, c):
    return a + x*b + x**2 * c

def main():
    path = "C:\MaskAligner\Data\ApproachCurve16_08_23.csv"
    save = ""
    no_err = False
    no_diff_err = False
    correlation = True
    plot_correlation = True
    
    #Simple command line parsing
    if("--file" in sys.argv or "-f" in sys.argv):
        for i in range(len(sys.argv)):
            if(sys.argv[i] == "--file" or sys.argv[i] == "-f"):
                try:
                    path = sys.argv[i+1]
                except:
                    print("incorrect file path.")
                    return
                    
    if("--save" in sys.argv or "-s" in sys.argv):
        for i in range(len(sys.argv)):
            if(sys.argv[i] == "--save" or sys.argv[i] == "-s"):
                try:
                    save = sys.argv[i+1]
                except:
                    print("incorrect save file name.")
                    return

    if("--no_diff_error" in sys.argv):
        no_diff_err = True
        
    if("--no_error" in sys.argv):
        no_err = True

    if("--no_correlation" in sys.argv):
        correlation = False
        
    if("--plot_correlation" in sys.argv):
        plot_correlation = True

    if("--help" in sys.argv or "-h" in sys.argv):
        print("--file x or -f x: plots the approach curve of file at filepath x. File must be csv, txt and must be formatted: c1, c2, c3, c1_err, c2_err, c3_err, c1_raw, c2_raw, c3_raw, c1_raw_error, c2_raw_error, c3_raw_error, additional_info.")
        print("--save x or -s x: saves the resulting plot with filename x. If x is left blank it will determine filename from input file. Should come as last command, since otherwise it will interpret following command as filename.")
        print("--no_diff_err: Omits errorbars on the capacitance differences.")
        print("--no_err: Omits errorbars on all plots.")
        print("--no_correlation: skips c1/c2, c1/c3, c2/c3 correlation calculation.")
        print("--plot_correlation: plots c1 and c2 with correlated other capacitances. Does nothing if --no_correlation is also given.")
        return

    data = np.genfromtxt(path, delimiter=",")
    data = data.T
    
    c1 = data[0]
    c2 = data[1]
    c3 = data[2]
    
    c1_err = data[3]
    c2_err = data[4]
    c3_err = data[5]
    
    x = np.arange(0,len(c1), 1)
    
    #Throw out mismeasurements, where error is very large and values where c is NaN and where difference to neighbouring values is large
    c1_err_mean = np.nanmean(c1_err)
    mask_c1 = np.where(c1_err > c1_err_mean * 4, False, True)
    mask_c1_nan = np.where(np.isnan(c1), False, True)
    c2_err_mean = np.nanmean(c2_err)
    mask_c2 = np.where(c2_err > c2_err_mean * 4, False, True)
    mask_c2_nan = np.where(np.isnan(c2), False, True)
    c3_err_mean = np.nanmean(c3_err)
    mask_c3 = np.where(c3_err > c3_err_mean * 4, False, True)
    mask_c3_nan = np.where(np.isnan(c3), False, True)
    c1_dif = np.diff(c1)
    c2_dif = np.diff(c2)
    c3_dif = np.diff(c3)
    full_mask = np.logical_and(np.logical_and(np.logical_and(np.logical_and(np.logical_and(mask_c1, mask_c2), mask_c3), mask_c1_nan), mask_c2_nan), mask_c3_nan)
    c1 = c1[full_mask]
    c1_err = c1_err[full_mask]
    c2 = c2[full_mask]
    c2_err = c2_err[full_mask]
    c3 = c3[full_mask]
    c3_err = c3_err[full_mask]
    x = x[full_mask]
    
    #Ensure no 0 uncertainties for fitting purposes
    c1_err = np.where(c1_err == 0, np.nanmax(c1_err), c1_err)
    c2_err = np.where(c2_err == 0, np.nanmax(c2_err), c2_err)
    c3_err = np.where(c3_err == 0, np.nanmax(c3_err), c3_err)
    
    #Ensure no NaN values in errors and capacitances
    
    c1_err = np.where(np.isnan(c1_err), np.nanmax(c1_err), c1_err)
    c2_err = np.where(np.isnan(c2_err), np.nanmax(c2_err), c2_err)
    c3_err = np.where(np.isnan(c3_err), np.nanmax(c3_err), c3_err)

    try:
        step_s = data[12][0]
        step_s_err = data[12][1]
    except:
        step_s = 69
        step_s_err = 1
        print("No stepsizes specified! Assuming Hard Coded Default!")
    
    c_diff_1 = np.ones(len(c1) - 1)
    c_diff_2 = np.ones(len(c2) - 1)
    c_diff_3 = np.ones(len(c3) - 1)
    
    c_diff_1_err = np.ones(len(c1) - 1)
    c_diff_2_err = np.ones(len(c2) - 1)
    c_diff_3_err = np.ones(len(c3) - 1)
    
    for i in range(len(c1)-1):
        c_diff_1[i] = c1[i+1] - c1[i]
        c_diff_2[i] = c2[i+1] - c2[i]
        c_diff_3[i] = c3[i+1] - c3[i]
    
        c_diff_1_err[i] = np.sqrt(c1_err[i+1]**2 + c1_err[i]**2)
        c_diff_2_err[i] = np.sqrt(c2_err[i+1]**2 + c2_err[i]**2)
        c_diff_3_err[i] = np.sqrt(c3_err[i+1]**2 + c3_err[i]**2)
        
    #for i in range(len(c1)-5):
    #    popt_c1, pcov_c1 = curve_fit(linear, x[i:i+5], c1[i:i+5], sigma=c1_err[i:i+5])
    #    popt_c2, pcov_c2 = curve_fit(linear, x[i:i+5], c2[i:i+5], sigma=c2_err[i:i+5])
    #    popt_c3, pcov_c3 = curve_fit(linear, x[i:i+5], c3[i:i+5], sigma=c3_err[i:i+5])
        
    #    c_diff_1[i] = popt_c1[1] + popt_c1[2] * c1[i]
    #    c_diff_2[i] = popt_c2[1] + popt_c2[2] * c2[i]
    #    c_diff_3[i] = popt_c3[1] + popt_c3[2] * c3[i]
        
    #    c_diff_1_err[i] = np.sqrt(pcov_c1[1][1])
    #    c_diff_2_err[i] = np.sqrt(pcov_c2[1][1])
    #    c_diff_3_err[i] = np.sqrt(pcov_c3[1][1])
   
    del data

    lw = 0.75
    ms = 2
    
    fig, ax = plt.subplots(2, 3)
    fig.set_figheight(9)
    fig.set_figwidth(16)
    
    if not no_err:
        ax[0,0].errorbar(x, c1, c1_err, color="tab:red", marker="x", markersize=ms, linestyle="--", linewidth=lw, capsize=3)
    else:
        ax[0,0].plot(x, c1, color="tab:red", marker="x", markersize=ms, linestyle="--", linewidth=lw)
    ax[0,0].set_title("Capacitance 1")
    ax[0,0].set_xlabel("Step [" + str(step_s) + "±" + str(step_s_err) + " nm]")
    ax[0,0].set_ylabel("Capacitance [pF]")
    
    if not no_err:
        ax[0,1].errorbar(x, c2, c2_err, color="tab:green", marker="x", markersize=ms, linestyle="--", linewidth=lw, capsize=3)
    else:
        ax[0,1].plot(x, c2, color="tab:green", marker="x", markersize=ms, linestyle="--", linewidth=lw)
    ax[0,1].set_title("Capacitance 2")
    ax[0,1].set_xlabel("Step [" + str(step_s) + "±" + str(step_s_err) + " nm]")
    ax[0,1].set_ylabel("Capacitance [pF]")
    
    if not no_err:
        ax[0,2].errorbar(x, c3, c3_err, color="tab:blue", marker="x", markersize=ms, linestyle="--", linewidth=lw, capsize=3)
    else:
        ax[0,2].plot(x, c3, color="tab:blue", marker="x", markersize=ms, linestyle="--", linewidth=lw)
    ax[0,2].set_title("Capacitance 3")
    ax[0,2].set_xlabel("Step [" + str(step_s) + "±" + str(step_s_err) + " nm]")
    ax[0,2].set_ylabel("Capacitance [pF]")
    
    if not no_diff_err and not no_err:
        ax[1,0].errorbar(x[1:], c_diff_1, c_diff_1_err, color="tab:red", marker="x", markersize=ms, linestyle="--", linewidth=lw, capsize=3)
    else:
        ax[1,0].plot(x[1:], c_diff_1, color="tab:red", marker="x", markersize=ms, linestyle="--", linewidth=lw)
    ax[1,0].set_title("Diff Capacitance 1")
    ax[1,0].set_xlabel("Step [" + str(step_s) + "±" + str(step_s_err) + " nm]")
    ax[1,0].set_ylabel("Capacitance [pF]")
    #ax[1,0].set_ylim(-0.0025, 0.02)
    
    if not no_diff_err and not no_err:
        ax[1,1].errorbar(x[1:], c_diff_2, c_diff_2_err, color="tab:green", marker="x", markersize=ms, linestyle="--", linewidth=lw, capsize=3)
    else:
        ax[1,1].plot(x[1:], c_diff_2, color="tab:green", marker="x", markersize=ms, linestyle="--", linewidth=lw)
    ax[1,1].set_title("Diff Capacitance 2")
    ax[1,1].set_xlabel("Step [" + str(step_s) + "±" + str(step_s_err) + " nm]")
    ax[1,1].set_ylabel("Capacitance [pF]")
    #ax[1,1].set_ylim(-0.0015, 0.008)
    
    if not no_diff_err and not no_err:
        ax[1,2].errorbar(x[1:], c_diff_3, c_diff_3_err, color="tab:blue", marker="x", markersize=ms, linestyle="--", linewidth=lw, capsize=3)
    else:
        ax[1,2].plot(x[1:], c_diff_3, color="tab:blue", marker="x", markersize=ms, linestyle="--", linewidth=lw)
    ax[1,2].set_title("Diff Capacitance 3")
    ax[1,2].set_xlabel("Step [" + str(step_s) + "±" + str(step_s_err) + " nm]")
    ax[1,2].set_ylabel("Capacitance [pF]")
    #ax[1,2].set_ylim(-0.002, 0.005)
    
    fig.tight_layout()
    plt.show()
    
    #Determine Savefile name if not already specified
    if(save == ""):
        path_parts = []
        if("\\" in path):
            path_parts = path.split("\\")
        elif ("/" in path):
            path_parts = path.split("/")
            
        save = "Plots/ApproachCurves/" + path_parts[-1].replace(".csv", "")
        
    fig.savefig(save + ".pdf")
    fig.savefig(save + ".png", dpi=300)

    if correlation:
        #Throw out outlier points
        c1_err_mean = np.mean(c1_err)
        mask_c1 = np.where(c1_err > c1_err_mean * 4, False, True)
        c2_err_mean = np.mean(c2_err)
        mask_c2 = np.where(c2_err > c2_err_mean * 4, False, True)
        c3_err_mean = np.mean(c3_err)
        mask_c3 = np.where(c3_err > c3_err_mean * 4, False, True)
        full_mask = np.logical_and(np.logical_and(mask_c1, mask_c2), mask_c3)
        c1 = c1[full_mask]
        c1_err = c1_err[full_mask]
        c2 = c2[full_mask]
        c2_err = c2_err[full_mask]
        c3 = c3[full_mask]
        c3_err = c3_err[full_mask]
        x = x[full_mask]
    
        #Determine correlation
        popt_1_2, pcov_1_2 = curve_fit(func, c2, c1, sigma=c2_err)
        popt_1_3, pcov_1_3 = curve_fit(func, c3, c1, sigma=c3_err)
        popt_2_3, pcov_2_3 = curve_fit(func, c3, c2, sigma=c3_err)
        
        chi_1_2 = np.sum(((c1 - func(c2, popt_1_2[0], popt_1_2[1]))**2/(c1_err**2 + (popt_1_2[1] * c2_err)**2))) / len(c1 - 2)
        chi_1_3 = np.sum(((c1 - func(c3, popt_1_3[0], popt_1_3[1]))**2/(c1_err**2 + (popt_1_3[1] * c3_err)**2))) / len(c1 - 2)
        chi_2_3 = np.sum(((c2 - func(c3, popt_2_3[0], popt_2_3[1]))**2/(c2_err**2 + (popt_2_3[1] * c3_err)**2))) / len(c2 - 2)
        
        print("Small chi^2/ndof means high correlation of different capacitances. Careful though! Check plot! Since if data is sufficiently linear they can always be fitted to each other.")
        print("chi^2/ndof similarity c1/c2: ", chi_1_2)
        print("chi^2/ndof similarity c1/c3: ", chi_1_3)
        print("chi^2/ndof similarity c2/c3: ", chi_2_3)
        
        if(plot_correlation):
            fig, ax = plt.subplots(2, 2)
            fig.set_figheight(9)
            fig.set_figwidth(16)
        
            c1_max = max(np.absolute(c1))
            c2_max = max(np.absolute(c2))
            
            fig.suptitle('Correlation of different capacitances')
            
            ax[0, 0].errorbar(x, func(c2, popt_1_2[0], popt_1_2[1]) / c1_max, func(c2_err, 0, np.absolute(popt_1_2[1])) / c1_max, label="C2 fit to C1", capsize=3, color="tab:green")
            ax[0, 0].errorbar(x, func(c3, popt_1_3[0], popt_1_3[1]) / c1_max, func(c3_err, 0, np.absolute(popt_1_3[1])) / c1_max, label="C3 fit to C1", capsize=3, color="tab:blue")
            ax[0, 0].errorbar(x, c1 / c1_max, c1_err/ c1_max, label="C1", capsize=3, color="tab:red",zorder=10)
            ax[0, 0].set_xlabel(r"steps")
            ax[0, 0].set_ylabel(r"$\frac{C}{C_{1,max}}$")
            ax[0, 0].legend()
            
            ax[0, 1].errorbar(x, func(c3, popt_2_3[0], popt_2_3[1]) / c2_max, func(c3_err, 0, np.absolute(popt_2_3[1])) / c2_max, label="C3 fit to C2", capsize=3, color="tab:blue")
            ax[0, 1].errorbar(x, c2 / c2_max, c2_err / c2_max, label="C2", capsize=3, color="tab:green",zorder=10)
            ax[0, 1].set_xlabel(r"steps")
            ax[0, 1].set_ylabel(r"$\frac{C}{C_{2,max}}$")
            ax[0, 1].legend()

            y_lim = max(max(np.absolute(c1 - func(c2, popt_1_2[0], popt_1_2[1])) / c1_max + func(c2_err, 0, np.absolute(popt_1_2[1])) / c1_max) * 1.1, max(np.absolute(c1 - func(c3, popt_1_3[0], popt_1_3[1])) / c1_max + func(c3_err, 0, np.absolute(popt_1_3[1])) / c1_max) * 1.1)
            
            ax[1, 0].errorbar(x, c1 / c1_max - func(c2, popt_1_2[0], popt_1_2[1]) / c1_max, func(c2_err, 0, np.absolute(popt_1_2[1])) / c1_max, label="C2 fit to C1", capsize=3, color="tab:green")
            ax[1, 0].errorbar(x, c1 / c1_max - func(c3, popt_1_3[0], popt_1_3[1]) / c1_max, func(c3_err, 0, np.absolute(popt_1_3[1])) / c1_max, label="C3 fit to C1", capsize=3, color="tab:blue")
            ax[1, 0].plot(x, np.ones(len(c1)) * 0, label="C1", color="tab:red", zorder=10)
            ax[1, 0].set_ylim(-y_lim, y_lim)
            ax[1, 0].set_ylabel(r"$\frac{C_1}{C_{1,max}}$ - fit")
            ax[1, 0].set_xlabel(r"steps")
            ax[1, 0].legend()
            
            ax[1, 1].errorbar(x, c2 / c2_max - func(c3, popt_2_3[0], popt_2_3[1]) / c2_max, func(c3_err, 0, np.absolute(popt_2_3[1])) / c2_max, label="C3 fit to C2", capsize=3, color="tab:blue")
            ax[1, 1].set_ylim(- max(np.absolute(c2 / c2_max - func(c3, popt_2_3[0], popt_2_3[1]) / c2_max + func(c3_err, 0, np.absolute(popt_2_3[1])) / c2_max)) * 1.1, max(np.absolute(c2 / c2_max - func(c3, popt_2_3[0], popt_2_3[1]) / c2_max + func(c3_err, 0, np.absolute(popt_2_3[1])) / c2_max)) * 1.1)
            ax[1, 1].plot(x, np.ones(len(c2)) * 0, label="C2", color="tab:green",zorder=10)
            ax[1, 1].set_ylabel(r"$\frac{C_2}{C_{2,max}}$ - fit")
            ax[1, 1].set_xlabel(r"steps")
            ax[1, 1].legend()
            plt.show()
    
    
if __name__ == "__main__":
    main()