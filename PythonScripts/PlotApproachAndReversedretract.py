import numpy as np
import matplotlib.pyplot as plt

def datahandling(data_app):
    c1_app = data_app[0]
    c2_app = data_app[1]
    c3_app = data_app[2]
    
    c1_app_err = data_app[3]
    c2_app_err = data_app[4]
    c3_app_err = data_app[5]
    
    x = np.arange(0, len(c1_app), 1.0)
    
    #Throw out mismeasurements, where error is very large and values where c is NaN
    c1_app_err_mean = np.nanmean(c1_app_err)
    mask_c1_app = np.where(c1_app_err > c1_app_err_mean * 4, False, True)
    mask_c1_app_nan = np.where(np.isnan(c1_app), False, True)
    c2_app_err_mean = np.nanmean(c2_app_err)
    mask_c2_app = np.where(c2_app_err > c2_app_err_mean * 4, False, True)
    mask_c2_app_nan = np.where(np.isnan(c2_app), False, True)
    c3_app_err_mean = np.nanmean(c3_app_err)
    mask_c3_app = np.where(c3_app_err > c3_app_err_mean * 4, False, True)
    mask_c3_app_nan = np.where(np.isnan(c3_app), False, True)
    full_mask = np.logical_and(np.logical_and(np.logical_and(np.logical_and(np.logical_and(mask_c1_app, mask_c2_app), mask_c3_app), mask_c1_app_nan), mask_c2_app_nan), mask_c3_app_nan)
    c1_app = c1_app[full_mask]
    c1_app_err = c1_app_err[full_mask]
    c2_app = c2_app[full_mask]
    c2_app_err = c2_app_err[full_mask]
    c3_app = c3_app[full_mask]
    c3_app_err = c3_app_err[full_mask]
    x = x[full_mask]
    
    #Ensure no 0 uncertainties for fitting purposes
    c1_app_err = np.where(c1_app_err == 0, np.nanmax(c1_app_err), c1_app_err)
    c2_app_err = np.where(c2_app_err == 0, np.nanmax(c2_app_err), c2_app_err)
    c3_app_err = np.where(c3_app_err == 0, np.nanmax(c3_app_err), c3_app_err)
    
    #Ensure no NaN values in errors and capacitances
    
    c1_app_err = np.where(np.isnan(c1_app_err), np.nanmax(c1_app_err), c1_app_err)
    c2_app_err = np.where(np.isnan(c2_app_err), np.nanmax(c2_app_err), c2_app_err)
    c3_app_err = np.where(np.isnan(c3_app_err), np.nanmax(c3_app_err), c3_app_err)

    try:
        step_s = data_app[12][0]
        step_s_err = data_app[12][1]
    except:
        step_s = 69
        step_s_err = 1
        print("No stepsizes specified! Assuming Hard Coded Default!")
    
    c_diff_1 = np.ones(len(c1_app) - 1)
    c_diff_2 = np.ones(len(c2_app) - 1)
    c_diff_3 = np.ones(len(c3_app) - 1)
    
    c_diff_1_err = np.ones(len(c1_app) - 1)
    c_diff_2_err = np.ones(len(c2_app) - 1)
    c_diff_3_err = np.ones(len(c3_app) - 1)
    
    for i in range(len(c1_app)-1):
        c_diff_1[i] = c1_app[i+1] - c1_app[i]
        c_diff_2[i] = c2_app[i+1] - c2_app[i]
        c_diff_3[i] = c3_app[i+1] - c3_app[i]
    
        c_diff_1_err[i] = np.sqrt(c1_app_err[i+1]**2 + c1_app_err[i]**2)
        c_diff_2_err[i] = np.sqrt(c2_app_err[i+1]**2 + c2_app_err[i]**2)
        c_diff_3_err[i] = np.sqrt(c3_app_err[i+1]**2 + c3_app_err[i]**2)
        
    return x, step_s, step_s_err, c1_app, c2_app, c3_app, c1_app_err, c2_app_err, c3_app_err, c_diff_1, c_diff_2, c_diff_3, c_diff_1_err, c_diff_2_err, c_diff_3_err

def main():
    no_err = False
    no_diff_err = False

    data_app = np.genfromtxt("C:\MaskAligner\Data\ApproachCurve21_11_23_ToFullContact.csv", delimiter=",").T
    data_retr = np.genfromtxt("C:\MaskAligner\Data\RetractCurve_21_11_23_600.csv", delimiter=",").T
    
    x, step_s, step_s_err, c1, c2, c3, c1_err, c2_err, c3_err, c_diff_1, c_diff_2, c_diff_3, c_diff_1_err, c_diff_2_err, c_diff_3_err = datahandling(data_app)
    x_retr, _, _, c1_retr, c2_retr, c3_retr, c1_err_retr, c2_err_retr, c3_err_retr, c_diff_1_retr, c_diff_2_retr, c_diff_3_retr, c_diff_1_err_retr, c_diff_2_err_retr, c_diff_3_err_retr = datahandling(data_retr)
    
    lw = 0.75
    ms = 2
    
    x_retr *= 1.18
    
    fig, ax = plt.subplots(2, 3)
    fig.set_figheight(9)
    fig.set_figwidth(16)
    
    if not no_err:
        ax[0,0].errorbar(x, c1, c1_err, color="tab:red", marker="x", markersize=ms, linestyle="--", linewidth=lw, capsize=3, label="approach")
        ax[0,0].errorbar(x_retr, c1_retr[::-1], c1_err_retr[::-1], color="tab:orange", marker="x", markersize=ms, linestyle="--", linewidth=lw, capsize=3, label="retract reversed")
    else:
        ax[0,0].plot(x, c1, color="tab:red", marker="x", markersize=ms, linestyle="--", linewidth=lw)
    ax[0,0].set_title("Capacitance 1")
    ax[0,0].set_xlabel("Step [" + str(step_s) + "±" + str(step_s_err) + " nm]")
    ax[0,0].set_ylabel("Capacitance [pF]")
    
    if not no_err:
        ax[0,1].errorbar(x, c2, c2_err, color="tab:green", marker="x", markersize=ms, linestyle="--", linewidth=lw, capsize=3, label="approach")
        ax[0,1].errorbar(x_retr, c2_retr[::-1], c2_err_retr[::-1], color="tab:olive", marker="x", markersize=ms, linestyle="--", linewidth=lw, capsize=3, label="retract reversed")
    else:
        ax[0,1].plot(x, c2, color="tab:green", marker="x", markersize=ms, linestyle="--", linewidth=lw)
    ax[0,1].set_title("Capacitance 2")
    ax[0,1].set_xlabel("Step [" + str(step_s) + "±" + str(step_s_err) + " nm]")
    ax[0,1].set_ylabel("Capacitance [pF]")
    
    if not no_err:
        ax[0,2].errorbar(x, c3, c3_err, color="tab:blue", marker="x", markersize=ms, linestyle="--", linewidth=lw, capsize=3, label="approach")
        ax[0,2].errorbar(x_retr, c3_retr[::-1], c3_err_retr[::-1], color="tab:cyan", marker="x", markersize=ms, linestyle="--", linewidth=lw, capsize=3, label="retract reversed")
    else:
        ax[0,2].plot(x, c3, color="tab:blue", marker="x", markersize=ms, linestyle="--", linewidth=lw)
    ax[0,2].set_title("Capacitance 3")
    ax[0,2].set_xlabel("Step [" + str(step_s) + "±" + str(step_s_err) + " nm]")
    ax[0,2].set_ylabel("Capacitance [pF]")
    
    if not no_diff_err and not no_err:
        ax[1,0].errorbar(x[1:], c_diff_1, c_diff_1_err, color="tab:red", marker="x", markersize=ms, linestyle="--", linewidth=lw, capsize=3, label="approach")
        ax[1,0].errorbar(x_retr[1:], c_diff_1_retr[::-1], c_diff_1_err_retr[::-1], color="tab:orange", marker="x", markersize=ms, linestyle="--", linewidth=lw, capsize=3, label="retract reversed")
    else:
        ax[1,0].plot(x[1:], c_diff_1, color="tab:red", marker="x", markersize=ms, linestyle="--", linewidth=lw)
    ax[1,0].set_title("Diff Capacitance 1")
    ax[1,0].set_xlabel("Step [" + str(step_s) + "±" + str(step_s_err) + " nm]")
    ax[1,0].set_ylabel("Capacitance [pF]")
    #ax[1,0].set_ylim(-0.0025, 0.02)
    
    if not no_diff_err and not no_err:
        ax[1,1].errorbar(x[1:], c_diff_2, c_diff_2_err, color="tab:green", marker="x", markersize=ms, linestyle="--", linewidth=lw, capsize=3, label="approach")
        ax[1,1].errorbar(x_retr[1:], c_diff_2_retr[::-1], c_diff_2_err_retr[::-1], color="tab:olive", marker="x", markersize=ms, linestyle="--", linewidth=lw, capsize=3, label="retract reversed")
    else:
        ax[1,1].plot(x[1:], c_diff_2, color="tab:green", marker="x", markersize=ms, linestyle="--", linewidth=lw)
    ax[1,1].set_title("Diff Capacitance 2")
    ax[1,1].set_xlabel("Step [" + str(step_s) + "±" + str(step_s_err) + " nm]")
    ax[1,1].set_ylabel("Capacitance [pF]")
    #ax[1,1].set_ylim(-0.0015, 0.008)
    
    if not no_diff_err and not no_err:
        ax[1,2].errorbar(x[1:], c_diff_3, c_diff_3_err, color="tab:blue", marker="x", markersize=ms, linestyle="--", linewidth=lw, capsize=3, label="approach")
        ax[1,2].errorbar(x_retr[1:], c_diff_3_retr[::-1], c_diff_3_err_retr[::-1], color="tab:cyan", marker="x", markersize=ms, linestyle="--", linewidth=lw, capsize=3, label="retract reversed")
    else:
        ax[1,2].plot(x[1:], c_diff_3, color="tab:blue", marker="x", markersize=ms, linestyle="--", linewidth=lw)
    ax[1,2].set_title("Diff Capacitance 3")
    ax[1,2].set_xlabel("Step [" + str(step_s) + "±" + str(step_s_err) + " nm]")
    ax[1,2].set_ylabel("Capacitance [pF]")
    #ax[1,2].set_ylim(-0.002, 0.005)
    
    ax[0,0].legend()
    ax[0,1].legend()
    ax[0,2].legend()
    ax[1,0].legend()
    ax[1,1].legend()
    ax[1,2].legend()
    
    fig.tight_layout()
    plt.show()
        
    
if __name__ == "__main__":
    main()
    