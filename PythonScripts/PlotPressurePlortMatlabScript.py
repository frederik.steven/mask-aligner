# -*- coding: utf-8 -*-
"""
Created on Fri Jan 26 13:18:01 2024

@author: admin
"""

import numpy as np
import matplotlib.pyplot as plt

import matplotlib.pyplot as plt
import numpy as np
import sys
from datetime import datetime
from dateutil import parser

def main():
    file = "C:\\MaskAligner\\Matlab\\test20.12.23\\Data_25-Jan-2024_10-16-48.csv"
    if("--file" in sys.argv or "-f" in sys.argv):
        for i in range(len(sys.argv)):
            if("--file" in sys.argv[i] or "-f" == sys.argv[i]):
                try:
                    file = sys.argv[i+1]
                except:
                    print("No file or incorrect file specification.")
                   
    times = []
    times_sec = []
    pressures = []
    
    with open(file, "r") as read_file:
        lines = read_file.readlines() 
        #pressures = np.zeros(len(lines) - 1)
        
        for i in range(1, len(lines)):
            temp = lines[i].split(",")
            if(i == 1):
                for j in range(len(temp) - 3): 
                    pressures.append(np.zeros(len(lines) - 1))
            
            t = parser.parse(temp[0].replace("  ", " "))
            times.append(t)
            times_sec.append(temp[1])
            for j in range(len(temp) - 3):
                pressures[j][i-1] = float(temp[j+3])
           
    for pressure in pressures:
        plt.plot(times, pressure)
    plt.ylabel("pressure [mbar]")
    plt.yscale("log")
    plt.show()

if __name__ == "__main__":
    main()