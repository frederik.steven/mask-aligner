import matplotlib.pyplot as plt
import numpy as np
import sys
from datetime import datetime

def remove_day_of_week(string):
    return string.replace("Mon", "").replace("Tue", "").replace("Wed", "").replace("Thu", "").replace("Fri", "").replace("Sat", "").replace("Son", "")

def main():
    file = "C:\MaskAligner\Data\Pressure\Pressure01.txt"
    if("--file" in sys.argv or "-f" in sys.argv):
        for i in range(len(sys.argv)):
            if("--file" in sys.argv[i] or "-f" == sys.argv[i]):
                try:
                    file = sys.argv[i+1]
                except:
                    print("No file or incorrect file specification.")

    times = []
    pressures = []
    
    with open(file, "r") as read_file:
        lines = read_file.readlines() 
        pressures = np.zeros(len(lines) - 1)
        
        for i in range(1, len(lines)):
            temp = lines[i].split("\t")
            t = datetime.strptime(remove_day_of_week(temp[0]).replace("  ", " "), "%H:%M:%S %d.%m.%y")
            times.append(t)
            pressures[i-1] = float(temp[1])
            
    plt.plot(times, pressures)
    plt.ylabel("pressure [mbar]")
    plt.show()

if __name__ == "__main__":
    main()