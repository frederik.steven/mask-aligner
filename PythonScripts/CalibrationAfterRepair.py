import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import matplotlib.font_manager as font_manager
from matplotlib.offsetbox import AnchoredText
import sys
from sigfig import round

#plt.rcParams['font.family'] = 'monospace'

class var_with_err:
    def __init__(self, var, err):
        self.var = var
        self.err = err
    
    def __str__(self):
        return round(self.var, uncertainty=self.err)

def linear(x, b):
    return b*x

def FitAndPlot(x, z1p, z1m, z2p, z2m, z3p, z3m, z1p_err, z1m_err, z2p_err, z2m_err, z3p_err, z3m_err, voltage, sigma, plots = True, raw = False, residuals=True):
    
    font = font_manager.FontProperties(family='DejaVu Sans Mono', style='normal')
    
    fac = 1e6
    
    popt_z1p, pcov_z1p = curve_fit(linear, x, z1p, sigma=z1p_err)
    popt_z1m, pcov_z1m = curve_fit(linear, x, z1m, sigma=z1m_err) 
    popt_z2p, pcov_z2p = curve_fit(linear, x, z2p, sigma=z2p_err) 
    popt_z2m, pcov_z2m = curve_fit(linear, x, z2m, sigma=z2m_err)
    popt_z3p, pcov_z3p = curve_fit(linear, x, z3p, sigma=z3p_err) 
    popt_z3m, pcov_z3m = curve_fit(linear, x, z3m, sigma=z3m_err) 
    
    step_z1p = var_with_err(popt_z1p[0] * fac, np.sqrt(pcov_z1p[0][0]) * fac)
    step_z1m = var_with_err(popt_z1m[0] * fac, np.sqrt(pcov_z1m[0][0]) * fac)
    step_z2p = var_with_err(popt_z2p[0] * fac, np.sqrt(pcov_z2p[0][0]) * fac)
    step_z2m = var_with_err(popt_z2m[0] * fac, np.sqrt(pcov_z2m[0][0]) * fac)
    step_z3p = var_with_err(popt_z3p[0] * fac, np.sqrt(pcov_z3p[0][0]) * fac)
    step_z3m = var_with_err(popt_z3m[0] * fac, np.sqrt(pcov_z3m[0][0]) * fac)
    
    chi_z1p = np.sum(((z1p - linear(x, popt_z1p[0]))/z1p_err)**2) / len(x - 2)
    chi_z1m = np.sum(((z1m - linear(x, popt_z1m[0]))/z1m_err)**2) / len(x - 2)
    chi_z2p = np.sum(((z2p - linear(x, popt_z2p[0]))/z2p_err)**2) / len(x - 2)
    chi_z2m = np.sum(((z2m - linear(x, popt_z2m[0]))/z2m_err)**2) / len(x - 2)
    chi_z3p = np.sum(((z3p - linear(x, popt_z3p[0]))/z3p_err)**2) / len(x - 2)
    chi_z3m = np.sum(((z3m - linear(x, popt_z3m[0]))/z3m_err)**2) / len(x - 2)

    err_z3z2p = popt_z3p[0] * fac / (popt_z2p[0] * fac)
    err_z3z2m = popt_z3m[0] * fac / (popt_z2m[0] * fac)
    err_z3z1p = popt_z3p[0] * fac / (popt_z1p[0] * fac)
    err_z3z1m = popt_z3m[0] * fac / (popt_z1m[0] * fac)
    err_z2z1p = popt_z2p[0] * fac / (popt_z1p[0] * fac)
    err_z2z1m = popt_z2m[0] * fac / (popt_z1m[0] * fac)
    
    print("### Voltage: ", voltage, " ###")
    
    print("z1p: ", step_z1p, " nm/step ", "chi^2/ndof = ", chi_z1p)
    print("z1m: ", step_z1m, " nm/step ", "chi^2/ndof = ", chi_z1m)
    print("z2p: ", step_z2p, " nm/step ", "chi^2/ndof = ", chi_z2p)
    print("z2m: ", step_z2m, " nm/step ", "chi^2/ndof = ", chi_z2m)
    print("z3p: ", step_z3p, " nm/step ", "chi^2/ndof = ", chi_z3p)
    print("z3m: ", step_z3m, " nm/step ", "chi^2/ndof = ", chi_z3m)
    
    print("error (ratio) + z3/z2: ", popt_z3p[0] * fac / (popt_z2p[0] * fac))
    print("error (ratio) - z3/z2: ", popt_z3m[0] * fac / (popt_z2m[0] * fac))
    print("error (ratio) + z3/z1: ", popt_z3p[0] * fac / (popt_z1p[0] * fac))
    print("error (ratio) - z3/z1: ", popt_z3m[0] * fac / (popt_z1m[0] * fac))
    print("error (ratio) + z2/z1: ", popt_z2p[0] * fac / (popt_z1p[0] * fac))
    print("error (ratio) - z2/z1: ", popt_z2m[0] * fac / (popt_z1m[0] * fac))


    if plots:
        
        x_inter = np.arange(min(x), max(x), 1) 
        l_jus = 5
        
        if not residuals:
            fig = plt.figure()
        else:
            fig, ax = plt.subplots(2,1, sharex=True, gridspec_kw={'height_ratios': [3,1]})
        
        fig.set_figheight(4.5)
        fig.set_figwidth(8)
        
        if residuals:
        
            ax[0].set_title("Mask Aligner Motors at " + str(voltage) + " V")
            ax[0].errorbar(x + 80, z1p, z1p_err, marker=".", linestyle="none", capsize=3, label="z1+:".ljust(l_jus ) + str(step_z1p) + " nm/step ", color="tab:blue")
            ax[0].errorbar(x + 40, z2p, z2p_err, marker=".", linestyle="none", capsize=3, label="z2+:".ljust(l_jus ) + str(step_z2p) + " nm/step ", color="tab:orange")
            ax[0].errorbar(x - 40, z3p, z3p_err, marker=".", linestyle="none", capsize=3, label="z3+:".ljust(l_jus ) + str(step_z3p) + " nm/step ", color="tab:purple")
            
            ax[0].plot(x_inter, linear(x_inter, popt_z1p[0]), color="tab:blue")
            ax[0].plot(x_inter, linear(x_inter, popt_z1m[0]), color="tab:cyan")
    
            ax[0].plot(x_inter, linear(x_inter, popt_z2p[0]), color="tab:orange")
            ax[0].plot(x_inter, linear(x_inter, popt_z2m[0]), color="tab:red")
            
            ax[0].errorbar(x - 80, z1m, z1m_err, marker=".", linestyle="none", capsize=3, label="z1-:".ljust(l_jus ) + str(step_z1m) + " nm/step ", color="tab:cyan")
            ax[0].errorbar(x + 120, z2m, z2m_err, marker=".", linestyle="none", capsize=3, label="z2-:".ljust(l_jus ) + str(step_z2m) + " nm/step ", color="tab:red")
            ax[0].errorbar(x, z3m, z3m_err, marker=".", linestyle="none", capsize=3, label="z3-:".ljust(l_jus ) + str(step_z3m) + " nm/step ", color="tab:pink")
            ax[0].plot(x_inter, linear(x_inter, popt_z3p[0]), color="tab:purple")
            ax[0].plot(x_inter, linear(x_inter, popt_z3m[0]), color="tab:pink")
            
            ax[0].set_xlabel("Number of steps driven")
            ax[0].set_ylabel("Distance travelled [mm]")
            ax[0].legend(handletextpad=0.1, prop = font)
            ax[0].grid()
            plt.tight_layout()
        
            #ax[1].set_title("residuals")
            ax[1].errorbar(x + 80, z1p - linear(x, popt_z1p[0]), z1p_err, marker=".", linestyle="none", capsize=3, label="z1+:".ljust(l_jus ) + str(step_z1p) + " nm/step ", color="tab:blue")
            ax[1].errorbar(x + 40, z2p - linear(x, popt_z2p[0]), z2p_err, marker=".", linestyle="none", capsize=3, label="z2+:".ljust(l_jus ) + str(step_z2p) + " nm/step ", color="tab:orange")
            ax[1].errorbar(x- 40, z3p - linear(x, popt_z3p[0]), z3p_err, marker=".", linestyle="none", capsize=3, label="z3+:".ljust(l_jus ) + str(step_z3p) + " nm/step ", color="tab:purple")
            
            ax[1].plot(x_inter, np.zeros(len(x_inter)), color="tab:blue")
            ax[1].set_ylim(0 - max(np.absolute(z3m - linear(x, popt_z1m[0]))) - max(z3m_err) - 0.05, 0 + max(np.absolute(z3m - linear(x, popt_z1m[0]))) + max(z3m_err)+ 0.05)
            #ax[1].plot(x_inter, linear(x_inter, popt_z1m[0]), color="tab:cyan")
    
            #ax[1].plot(x_inter, linear(x_inter, popt_z2p[0]), color="tab:orange")
            #ax[1].plot(x_inter, linear(x_inter, popt_z2m[0]), color="tab:red")
            
            ax[1].errorbar(x - 80, z1m - linear(x, popt_z1m[0]), z1m_err, marker=".", linestyle="none", capsize=3, label="z1-:".ljust(l_jus ) + str(step_z1m) + " nm/step ", color="tab:cyan")
            ax[1].errorbar(x + 120, z2m - linear(x, popt_z1m[0]), z2m_err, marker=".", linestyle="none", capsize=3, label="z2-:".ljust(l_jus ) + str(step_z2m) + " nm/step ", color="tab:red")
            ax[1].errorbar(x, z3m - linear(x, popt_z1m[0]), z3m_err, marker=".", linestyle="none", capsize=3, label="z3-:".ljust(l_jus ) + str(step_z3m) + " nm/step ", color="tab:pink")
            #ax[1].plot(x_inter, linear(x_inter, popt_z3p[0]), color="tab:purple")
            #ax[1].plot(x_inter, linear(x_inter, popt_z3m[0]), color="tab:pink")
            
            ax[1].set_xlabel("Number of steps driven")
            ax[1].set_ylabel("Distance travelled [mm]")
            
            ax[1].grid()
            
        else:
            
            plt.title("Mask Aligner Motors at " + str(voltage) + " V")
            plt.errorbar(x + 80, z1p, z1p_err, marker=".", linestyle="none", capsize=3, label="z1+:".ljust(l_jus ) + str(step_z1p) + " nm/step ", color="tab:blue")
            plt.errorbar(x + 40, z2p, z2p_err, marker=".", linestyle="none", capsize=3, label="z2+:".ljust(l_jus ) + str(step_z2p) + " nm/step ", color="tab:orange")
            plt.errorbar(x - 40, z3p, z3p_err, marker=".", linestyle="none", capsize=3, label="z3+:".ljust(l_jus ) + str(step_z3p) + " nm/step ", color="tab:purple")
            
            plt.plot(x_inter, linear(x_inter, popt_z1p[0]), color="tab:blue")
            plt.plot(x_inter, linear(x_inter, popt_z1m[0]), color="tab:cyan")
    
            plt.plot(x_inter, linear(x_inter, popt_z2p[0]), color="tab:orange")
            plt.plot(x_inter, linear(x_inter, popt_z2m[0]), color="tab:red")
            
            plt.errorbar(x - 80, z1m, z1m_err, marker=".", linestyle="none", capsize=3, label="z1-:".ljust(l_jus ) + str(step_z1m) + " nm/step ", color="tab:cyan")
            plt.errorbar(x + 120, z2m, z2m_err, marker=".", linestyle="none", capsize=3, label="z2-:".ljust(l_jus ) + str(step_z2m) + " nm/step ", color="tab:red")
            plt.errorbar(x, z3m, z3m_err, marker=".", linestyle="none", capsize=3, label="z3-:".ljust(l_jus ) + str(step_z3m) + " nm/step ", color="tab:pink")
            plt.plot(x_inter, linear(x_inter, popt_z3p[0]), color="tab:purple")
            plt.plot(x_inter, linear(x_inter, popt_z3m[0]), color="tab:pink")
            
            plt.xlabel("Number of steps driven")
            plt.ylabel("Distance travelled [mm]")
            plt.legend(handletextpad=0.1, prop = font)
            plt.grid()
            plt.tight_layout()
                
        plt.savefig("Plots/CalibrationAfterRepair/" + str(voltage) + "V.pdf")
        plt.savefig("Plots/CalibrationAfterRepair/" + str(voltage) + "V.png", dpi=300)
        plt.show()
        
    return step_z1p, step_z1m, step_z2p, step_z2m, step_z3p, step_z3m, err_z3z2p, err_z3z2m, err_z3z1p, err_z3z1m, err_z2z1p, err_z2z1m   

def dataHandling(data, offset, voltage, plots, raw):
    x = data[0 + offset]
    z3m = data[1 + offset]
    z3p = data[2 + offset]
    z2m = data[3 + offset]
    z2p = data[4 + offset]
    z1m = data[5 + offset]
    z1p = data[6 + offset]
    
    z3m_err = data[7 + offset]
    z3p_err = data[8 + offset]
    z2m_err = data[9 + offset]
    z2p_err = data[10 + offset]
    z1m_err = data[11 + offset]
    z1p_err = data[12 + offset]
    
    sigma = np.ones(len(x)) * data[0][0]
    
    #Check for empty data and fill with dummy
    if np.isnan(z1p).any():
        z1p = np.zeros(5)
    if np.isnan(z1m).any():
        z1m = np.zeros(5)
    if np.isnan(z2p).any():
        z2p = np.zeros(5)
    if np.isnan(z2m).any():
        z2m = np.zeros(5)
    if np.isnan(z3p).any():
        z3p = np.zeros(5)
    if np.isnan(z3m).any():
        z3m = np.zeros(5)
        
    if np.isnan(z1p_err).any():
        z1p_err = np.ones(5) * sigma
    if np.isnan(z1m_err).any():
        z1m_err = np.ones(5) * sigma
    if np.isnan(z2p_err).any():
        z2p_err = np.ones(5) * sigma
    if np.isnan(z2m_err).any():
        z2m_err = np.ones(5) * sigma
    if np.isnan(z3p_err).any():
        z3p_err = np.ones(5) * sigma
    if np.isnan(z3m_err).any():
        z3m_err = np.ones(5) * sigma
    
    step_z1p, step_z1m, step_z2p, step_z2m, step_z3p, step_z3m, err_z3z2p, err_z3z2m, err_z3z1p, err_z3z1m, err_z2z1p, err_z2z1m = FitAndPlot(x, z1p, z1m, z2p, z2m, z3p, z3m, z1p_err, z1m_err, z2p_err, z2m_err, z3p_err, z3m_err, voltage, sigma, plots, raw)
    return step_z1p, step_z1m, step_z2p, step_z2m, step_z3p, step_z3m, err_z3z2p, err_z3z2m, err_z3z1p, err_z3z1m, err_z2z1p, err_z2z1m
    
def main():
    plots = True
    raw = False

    file = "C:\MaskAligner\Data\Calibration\CalibrationAfterRepair_13_11_23.csv"

    # very Simple commandline parser
    if("--no_plots" in sys.argv):
        plots = False
    
    if("--file" in sys.argv or "-f" in sys.argv):
        for i in range(len(sys.argv)):
            if("--file" in sys.argv[i] or "-f" == sys.argv[i]):
                try:
                    file = sys.argv[i+1]
                except:
                    print("No file or incorrect file specification.")
    
    if("--raw_plot" in sys.argv):
        raw = True

    data = np.genfromtxt(file, skip_header=1, delimiter=",")
    data = data.T
    
    arr = []
    
    arr.append(dataHandling(data, 43, 70, plots, raw))
    arr.append(dataHandling(data, 1, 80, plots, raw))
    arr.append(dataHandling(data, 15, 100, plots, raw))
    arr.append(dataHandling(data, 29, 120, plots, raw))
    
    arr = np.array(arr).T
    
    ## Plot motors errors against one another against voltage 
    
    fig = plt.figure() 
    fig.set_figheight(4.5)
    fig.set_figwidth(8)
    
    plt.plot([70, 80, 100, 120], arr[6], label="error z3/z2+", marker=".", linestyle="dashed")
    plt.plot([70, 80, 100, 120], arr[8], label="error z3/z1+", marker=".", linestyle="dashed")
    plt.plot([70, 80, 100, 120], arr[10], label="error z2/z1+", marker=".", linestyle="dashed")
    plt.plot([70, 80, 100, 120], arr[7], label="error z3/z2-", marker=".", linestyle="dashed")
    plt.plot([70, 80, 100, 120], arr[9], label="error z3/z1-", marker=".", linestyle="dashed")
    plt.plot([70, 80, 100, 120], arr[11], label="error z2/z1-", marker=".", linestyle="dashed")
    plt.xlabel("Voltage [V]")
    plt.ylabel("ratio of motor's stepsizes")
    plt.legend()
    plt.tight_layout()
    plt.savefig("Plots/CalibrationAfterRepair/Errors.pdf")
    plt.savefig("Plots/CalibrationAfterRepair/Errors.png", dpi=300)
    plt.show()
    
    ## Plot stepsizes against voltage 
    fig = plt.figure() 
    fig.set_figheight(4.5)
    fig.set_figwidth(8)   
    
    plt.errorbar([70, 80, 100, 120], [x.var for x in arr[0]], [x.err for x in arr[0]], label="z1+", marker=".", linestyle="dashed", capsize=3, color="tab:blue")
    plt.errorbar([70, 80, 100, 120], [x.var for x in arr[1]], [x.err for x in arr[1]], label="z1-", marker=".", linestyle="dashed", capsize=3, color="tab:cyan")
    plt.errorbar([70, 80, 100, 120], [x.var for x in arr[2]], [x.err for x in arr[2]], label="z2+", marker=".", linestyle="dashed", capsize=3, color="tab:orange")
    plt.errorbar([70, 80, 100, 120], [x.var for x in arr[3]], [x.err for x in arr[3]], label="z2-", marker=".", linestyle="dashed", capsize=3, color="tab:red")
    plt.errorbar([70, 80, 100, 120], [x.var for x in arr[4]], [x.err for x in arr[4]], label="z3+", marker=".", linestyle="dashed", capsize=3, color="tab:purple")
    plt.errorbar([70, 80, 100, 120], [x.var for x in arr[5]], [x.err for x in arr[5]], label="z3-", marker=".", linestyle="dashed", capsize=3, color="tab:pink")
    plt.xlabel("Voltage [V]")
    plt.ylabel("stepsize [nm/step]")
    plt.legend()
    plt.tight_layout()
    plt.savefig("Plots/CalibrationAfterRepair/Stepsizes.pdf")
    plt.savefig("Plots/CalibrationAfterRepair/Stepsizes.png", dpi=300)
    plt.show()
    
    
    ## Plot up/down ratio against Voltage
    z1_ud = var_with_err(np.array([x.var for x in arr[0]])/np.array([x.var for x in arr[1]]), np.sqrt((1/np.array([x.var for x in arr[1]]) * np.array([x.err for x in arr[0]]))**2 + (np.array([x.var for x in arr[0]])/np.array([x.var for x in arr[1]])**2 * np.array([x.err for x in arr[1]]))**2))
    z2_ud = var_with_err(np.array([x.var for x in arr[2]])/np.array([x.var for x in arr[3]]), np.sqrt((1/np.array([x.var for x in arr[3]]) * np.array([x.err for x in arr[2]]))**2 + (np.array([x.var for x in arr[2]])/np.array([x.var for x in arr[3]])**2 * np.array([x.err for x in arr[3]]))**2))
    z3_ud = var_with_err(np.array([x.var for x in arr[4]])/np.array([x.var for x in arr[5]]), np.sqrt((1/np.array([x.var for x in arr[5]]) * np.array([x.err for x in arr[4]]))**2 + (np.array([x.var for x in arr[4]])/np.array([x.var for x in arr[5]])**2 * np.array([x.err for x in arr[5]]))**2))
    
    fig = plt.figure() 
    fig.set_figheight(4.5)
    fig.set_figwidth(8)   
    
    plt.errorbar([70, 80, 100, 120], z1_ud.var, z1_ud.err, label="z1", marker=".", linestyle="dashed", capsize=3, color="tab:blue")
    plt.errorbar([70, 80, 100, 120], z2_ud.var, z2_ud.err, label="z2", marker=".", linestyle="dashed", capsize=3, color="tab:cyan")
    plt.errorbar([70, 80, 100, 120], z3_ud.var, z3_ud.err, label="z3", marker=".", linestyle="dashed", capsize=3, color="tab:orange")
    plt.xlabel("Voltage [V]")
    plt.ylabel("ratio approach/retract")
    plt.legend()
    plt.tight_layout()
    plt.savefig("Plots/CalibrationAfterRepair/ratioUD.pdf")
    plt.savefig("Plots/CalibrationAfterRepair/ratioUD.png", dpi=300)
    plt.show()

if __name__ == "__main__":
    main()