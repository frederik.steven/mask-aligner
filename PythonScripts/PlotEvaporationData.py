import numpy as np
import matplotlib.pyplot as plt
import sys

def main():
    path = "C:\MaskAligner\Data\ApproachCurve16_08_23.csv"
    save = ""
    
    plot_Fil = False
    plot_Emis = False
    plot_Flux = False
    plot_Volt = False
    plot_Pressure = False
    plot_Temp = False
    
    if("--file" in sys.argv or "-f" in sys.argv):
        for i in range(len(sys.argv)):
            if(sys.argv[i] == "--file" or sys.argv[i] == "--f"):
                try:
                    path = sys.argv[i+1]
                except:
                    print("incorrect file path.")
                    return

    if("--FIL" in sys.argv):
        plot_Fil = True
    if("--EMIS" in sys.argv):
        plot_Emis = True
    if("--FLUX" in sys.argv):
        plot_Flux = True
    if("--Volt" in sys.argv):
        plot_Volt = True
    if("--Pressure" in sys.argv):
        plot_Pressure = True
    if("--Temp" in sys.argv):
        plot_Temp = True 
    if("--All" in sys.argv):
        plot_Temp = True
        plot_Pressure = True
        plot_Volt = True
        plot_Flux = True
        plot_Emis = True
        plot_Fil = True

    data = np.genfromtxt(path, delimiter=",", skip_header=1)
    data = data.T
    time = data[0]
    FIL = data[1]
    EMIS = data[2]
    FLUX = data[3]
    VOLT = data[4]
    Pressure = data[5]
    TEMP = data[6]
    
    if(plot_Fil):
        plt.plot(time, FIL, label="FIL")
        #plt.legend()
        plt.xlabel("time in min")
        plt.ylabel("FIL in A")
        plt.show()
    if(plot_Emis):
        plt.plot(time, EMIS, label="EMIS")
        #plt.legend()
        plt.xlabel("time in min")
        plt.ylabel("EMIS in mA")
        plt.show()
    if(plot_Flux):
        plt.plot(time, FLUX, label="Flux")
        #plt.legend()
        plt.xlabel("time in min")
        plt.ylabel("Flux in nA")
        plt.show()
    if(plot_Volt):
        plt.plot(time, VOLT, label="Volt")
        #plt.legend()
        plt.xlabel("time in min")
        plt.ylabel("Volt in V")
        plt.show()
    if(plot_Pressure):
        plt.plot(time, Pressure, label="Pressure")
        #plt.legend()
        plt.xlabel("time in min")
        plt.ylabel("Pressure in mbar")
        plt.show()
    if(plot_Temp):
        plt.plot(time, TEMP, label="Temp")
        #plt.legend()
        plt.xlabel("time in min")
        plt.ylabel("Temp in °C")
        plt.show()

    
if __name__ == "__main__":
    main()