# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 14:45:10 2023

@author: Luzifer
"""

import matplotlib.pyplot as plt
import numpy as np
import sys

def main():
    path = []
    save = ""
    
    #Simple command line parsing
    if("--files" in sys.argv or "-f" in sys.argv):
        for i in range(len(sys.argv)):
            if(sys.argv[i] == "--files" or sys.argv[i] == "--f"):
                for j in range(i+1, len(sys.argv)):
                    if(sys.argv[j] == "--save" or sys.argv[j] == "-s"):
                        break
                    try:
                        path.append(sys.argv[j])
                    except:
                        print("incorrect file path.")
                        return
                        
    if("--save" in sys.argv or "-s" in sys.argv):
        for i in range(len(sys.argv)):
            if(sys.argv[i] == "--save" or sys.argv[i] == "-s"):
                try:
                    save = sys.argv[i+1]
                except:
                    print("incorrect save file name.")
                    return

    if path == []:
        print("No data to plot.")
        return
    
    xs = []
    
    c1s = []
    c2s = []
    c3s = []
    
    c_diff_1s = []
    c_diff_2s = []
    c_diff_3s = []
    
    for i in range(len(path)):
        data = np.genfromtxt(path[i], delimiter=",")
        
        x = data.T[0]
        c1 = data.T[1]
        c2 = data.T[2]
        c3 = data.T[3]
        
        c_diff_1 = np.ones(len(c1) - 1)
        c_diff_2 = np.ones(len(c2) - 1)
        c_diff_3 = np.ones(len(c2) - 1)
        
        xs.append(x)
        c1s.append(c1)
        c2s.append(c2)
        c3s.append(c3)
        
        c_diff_1s.append(c_diff_1)
        c_diff_2s.append(c_diff_2)
        c_diff_3s.append(c_diff_3)
        
    for j in range(len(path)):
        for i in range(len(c1s[j])-1):
            c_diff_1s[j][i] = c1s[j][i+1] - c1s[j][i]
            c_diff_2s[j][i] = c2s[j][i+1] - c2s[j][i]
            c_diff_3s[j][i] = c3s[j][i+1] - c3s[j][i]
    
    del data
    del c1, c2, c3, c_diff_1, c_diff_2, c_diff_3
    
    lw = 0.75
    ms = 2
        
    fig, ax = plt.subplots(2, 3)
    fig.set_figheight(9)
    fig.set_figwidth(16)
    
    color_1 = ["tab:red", "#e24522", "#ec5f1a", "#f47711", "#fb8f05", "#ffa600"]
    color_2 = ["tab:green", "#62a50d", "#8ca900", "#b3aa00", "#d9a900", "#ffa600"]
    color_3 = ["tab:blue", "#7e75c8", "#cd6abc", "#ff6292", "#ff7957", "#ffa600"]
    
    for i in range(len(path)):
        x = np.arange(0, len(c1s[i]))
        
        ax[0,0].plot(xs[i], c1s[i], color=color_1[i], marker="x", markersize=ms, linestyle="--", linewidth=lw, label=path[i].replace("\\", "/").split("/")[-1].replace(".csv", ""))
        ax[0,0].set_title("Capacitance 1")
        ax[0,0].set_xlabel("Step")
        ax[0,0].set_ylabel("Capacitance [pF]")
        
        ax[0,1].plot(xs[i], c2s[i], color=color_2[i], marker="x", markersize=ms, linestyle="--", linewidth=lw, label=path[i].replace("\\", "/").split("/")[-1].replace(".csv", ""))
        ax[0,1].set_title("Capacitance 2")
        ax[0,1].set_xlabel("Step")
        ax[0,1].set_ylabel("Capacitance [pF]")
        
        ax[0,2].plot(xs[i], c3s[i], color=color_3[i], marker="x", markersize=ms, linestyle="--", linewidth=lw, label=path[i].replace("\\", "/").split("/")[-1].replace(".csv", ""))
        ax[0,2].set_title("Capacitance 3")
        ax[0,2].set_xlabel("Step")
        ax[0,2].set_ylabel("Capacitance [pF]")
        
        ax[1,0].plot(xs[i][1:], c_diff_1s[i], color=color_1[i], marker="x", markersize=ms, linestyle="--", linewidth=lw, label=path[i].replace("\\", "/").split("/")[-1].replace(".csv", ""))
        ax[1,0].set_title("Diff Capacitance 1")
        ax[1,0].set_xlabel("Step")
        ax[1,0].set_ylabel("Capacitance [pF]")
        #ax[1,0].set_ylim(-0.0025, 0.02)
        
        ax[1,1].plot(xs[i][1:], c_diff_2s[i], color=color_2[i], marker="x", markersize=ms, linestyle="--", linewidth=lw, label=path[i].replace("\\", "/").split("/")[-1].replace(".csv", ""))
        ax[1,1].set_title("Diff Capacitance 2")
        ax[1,1].set_xlabel("Step")
        ax[1,1].set_ylabel("Capacitance [pF]")
        #ax[1,1].set_ylim(-0.0015, 0.008)
        
        ax[1,2].plot(xs[i][1:], c_diff_3s[i], color=color_3[i], marker="x", markersize=ms, linestyle="--", linewidth=lw, label=path[i].replace("\\", "/").split("/")[-1].replace(".csv", ""))
        ax[1,2].set_title("Diff Capacitance 3")
        ax[1,2].set_xlabel("Step")
        ax[1,2].set_ylabel("Capacitance [pF]")
        #ax[1,2].set_ylim(-0.002, 0.005)
     
    ax[0,0].legend()
    ax[0,1].legend()
    ax[0,2].legend()
    ax[1,0].legend()
    ax[1,1].legend()
    ax[1,2].legend()
    ax[1,2].legend()
    fig.tight_layout()
    plt.show()
    
    #Determine Savefile name if not already specified, by taking capital letters and number in string and concatenating
    if(save == ""):
        path_parts = []
        for i in range(len(path)):
            path_parts.append(path[i].replace("\\", "/").split("/")[-1].replace(".csv", ""))
            path_parts[i] = [x for x in path_parts[i] if x.isupper() or x.isdigit()]
            path_parts[i] = "".join(path_parts[i])
        
        path_part = ""
        for i in range(len(path)):
            path_part += path_parts[i]
        
        save = "Plots/ApproachCurves/" + path_part
        
    fig.savefig(save + "comparison" + ".pdf")
    fig.savefig(save + "comparison" + ".png", dpi=300)

if __name__ == "__main__":
    main()