import numpy as np
import matplotlib.pyplot as plt
import sys

def main():
    if("--no_plots" in sys.argv):
        plots = False

    #plt.style.use('tableau-colorblind10')

    file = "C:/MaskAligner/Data/CalibrationScrew/Calib_04_10_23.txt"
    if("--file" in sys.argv or "-f" in sys.argv):
        for i in range(len(sys.argv)):
            if("--file" in sys.argv[i] or "-f" == sys.argv[i]):
                try:
                    file = sys.argv[i+1]
                except:
                    print("No file or incorrect file specification.")
                     
    factor = 9000
    data = np.genfromtxt(file, delimiter="\t", skip_header=1)
    data = data.T
    rot = data[0]
    z3m = data[1] / factor * 10**6
    z3p = data[2] / factor * 10**6
    z2m = data[3] / factor * 10**6
    z2p = data[4] / factor * 10**6
    
    z3mu = data[11] / factor * 10**6
    z3md = data[12] / factor * 10**6
    z3pu = data[13] / factor * 10**6
    z3pd = data[14] / factor * 10**6
    
    z3left01 = data[23]
    z3left02 = data[24]
    z3left03 = data[25]
    z3mid = data[26]
    z3right = data[27]
    z3right02 = data[28]
    
    z3left01_c = data[30]
    z3left02_c = data[31]
    z3mid_c = data[32]
    z3right_c = data[33]
    z3right02_c = data[34]
    
    rot_d = data[37] 
    z3m_d = data[38] / factor * 10**6
    z3p_d = data[40] / factor * 10**6
    z2m_d = data[39] / factor * 10**6
    z2p_d = data[41] / factor * 10**6
    
    rot_d = rot_d[~np.isnan(rot_d)]
    z3m_d = z3m_d[~np.isnan(z3m_d)]
    z3p_d = z3p_d[~np.isnan(z3p_d)]
    z2m_d = z2m_d[~np.isnan(z2m_d)]
    z2p_d = z2p_d[~np.isnan(z2p_d)]
    
    rot_dp = data[43]
    z3_dp_m = data[44] / factor * 10**6
    z3_dp_p = data[45] / factor * 10**6
    
    z3AngleX = [z3left01[0], z3left02[0], z3left03[0], z3mid[0], z3right[0], z3right02[0]]
    z3AngleX_copper = [z3left01_c[0], z3left02_c[0], z3mid_c[0], z3right_c[0], z3right02_c[0]]
    
    z_err = 0.04 / factor * 10**6
    
    z3p_vars = []
    z3m_vars = []
    
    rot_z3 = rot
    
    for i in range(1):
        z3p_vars.append(np.roll(z3p, i))
        z3m_vars.append(np.roll(z3m, i))
        
    for i in range(len(z3p_vars)):
        plt.errorbar(rot_z3, z3p_vars[i], z_err, label="z3+ " + str(i), marker=".", capsize=3)
        plt.errorbar(rot_z3, z3m_vars[i], z_err, label="z3- " + str(i), marker=".", capsize=3)
    plt.errorbar(rot, z2p, z_err, label="z2+", marker=".", capsize=3)
    plt.errorbar(rot, z2m, z_err, label="z2-", marker=".", capsize=3)
    plt.xlabel("screw rotations from arbitrary 0. Larger means less firm screw.")
    plt.ylabel("stepsize [nm]")
    
    plt.legend()
    plt.savefig("Plots/Z3Examine/ScrewRot.png", dpi=300)
    plt.savefig("Plots/Z3Examine/ScrewRot.pdf", dpi=300)
    plt.show()
    
    plt.plot(rot, z2p/z2m, label="z2 ratio", marker=".")
    plt.plot(rot, z3p/z3m, label="z3 ratio", marker=".")
    plt.xlabel("screw rotations from arbitrary 0. Larger means less firm screw.")
    plt.ylabel("ration up/down")
    plt.legend()
    plt.savefig("Plots/Z3Examine/UpDownRatio.png", dpi=300)
    plt.savefig("Plots/Z3Examine/UpDownRatio.pdf", dpi=300)
    plt.show()
    
    plt.errorbar(1 + 0.1, np.nanmean(z3mu), np.nanstd(z3mu, ddof=1), label="wobble up", capsize=3, marker=".")
    plt.errorbar(-1 - 0.1, np.nanmean(z3pu), np.nanstd(z3pu, ddof=1), label="wobble up", capsize=3, marker=".")
    plt.errorbar(1 - 0.1, np.nanmean(z3md), np.nanstd(z3md, ddof=1), label="wobble down", capsize=3, marker=".")
    plt.errorbar(-1 + 0.1, np.nanmean(z3pd), np.nanstd(z3pd, ddof=1), label="wobble down", capsize=3, marker=".")
    plt.xlabel("movement direction -1 = approach / +1 = retract")
    plt.ylabel("stepsize [nm]")
    plt.legend()
    plt.savefig("Plots/Z3Examine/ScrewWobble.png", dpi=300)
    plt.savefig("Plots/Z3Examine/ScrewWobble.pdf", dpi=300)
    plt.show()
    
    z3AngleY = np.array([np.nanmean(z3left01[1:]), np.nanmean(z3left02[1:]), np.nanmean(z3left03[1:]), np.nanmean(z3mid[1:]), np.nanmean(z3right[1:]), np.nanmean(z3right02[1:])]) / factor * 10**6
    z3AngleY_err = np.array([np.nanstd(z3left01[1:], ddof=1), np.nanstd(z3left02[1:], ddof=1), np.nanstd(z3left03[1:], ddof=1), np.nanstd(z3mid[1:], ddof=1), np.nanstd(z3right[1:], ddof=1), np.nanstd(z3right02[1:], ddof=1)]) / factor * 10**6
    
    z3AngleY_copper = np.array([np.nanmean(z3left01_c[1:]), np.nanmean(z3left02_c[1:]), np.nanmean(z3mid_c[1:]), np.nanmean(z3right_c[1:]), np.nanmean(z3right02_c[1:])]) / factor * 10**6
    z3AngleY_err_copper = np.array([np.nanstd(z3left01_c[1:], ddof=1), np.nanstd(z3left02_c[1:], ddof=1), np.nanstd(z3mid_c[1:], ddof=1), np.nanstd(z3right_c[1:], ddof=1), np.nanstd(z3right02_c[1:], ddof=1)]) / factor * 10**6
    
    plt.errorbar(z3AngleX, z3AngleY, z3AngleY_err, capsize=3, marker=".", linestyle="none", label="w/o copper wire")
    plt.errorbar(z3AngleX_copper, z3AngleY_copper, z3AngleY_err_copper, capsize=3, marker=".", linestyle="none", label="copper wire")
    plt.xlabel("rotation of front piezo plate [°]")
    plt.ylabel("stepsize [nm]")
    plt.legend()
    plt.savefig("Plots/Z3Examine/FrontPlateAngle.png", dpi=300)
    plt.savefig("Plots/Z3Examine/FrontPlateAngle.pdf", dpi=300)
    plt.show()
    
    plt.errorbar(rot_d, z3p_d, z_err, label="z3+ ", marker=".", capsize=3, color="tab:blue")
    plt.errorbar(rot_d, z3m_d, z_err, label="z3- ", marker=".", capsize=3, color="tab:orange")
    plt.errorbar(rot_d, z2p_d, z_err, label="z2+", marker=".", capsize=3, color="tab:green")
    plt.errorbar(rot_d, z2m_d, z_err, label="z2-", marker=".", capsize=3, color="tab:red")
    
    #plt.errorbar(rot_z3[:3], z3p_vars[0][:3], z_err, label="z3+ " + str(i), marker=".", capsize=3, color="blue")
    #plt.errorbar(rot_z3[:3], z3m_vars[0][:3], z_err, label="z3- " + str(i), marker=".", capsize=3, color="orange")
    #plt.errorbar(rot[:3], z2p[:3], z_err, label="z2+", marker=".", capsize=3, color="green")
    #plt.errorbar(rot[:3], z2m[:3], z_err, label="z2-", marker=".", capsize=3, color="red")
    
    plt.xlabel("screw rotations from arbitrary 0. Larger means less firm screw.")
    plt.ylabel("stepsize [nm]")
    plt.legend()
    plt.savefig("Plots/Z3Examine/ScrewRot_detail.png", dpi=300)
    plt.savefig("Plots/Z3Examine/ScrewRot_detail.pdf", dpi=300)
    plt.show()
    
    plt.errorbar(rot_z3, z3p_vars[0], z_err, label="z3+ ", marker=".", capsize=3)
    plt.errorbar(rot_z3, z3m_vars[0], z_err, label="z3- ", marker=".", capsize=3)
    plt.errorbar(-rot_dp + 5, z3_dp_p, z_err, label="z3+ with z1 front plate", marker=".", capsize=3)
    plt.errorbar(-rot_dp + 5, z3_dp_m, z_err, label="z3- with z1 front plate", marker=".", capsize=3)
    plt.errorbar(rot, z2p, z_err, label="z2+", marker=".", capsize=3)
    plt.errorbar(rot, z2m, z_err, label="z2-", marker=".", capsize=3)
    plt.xlabel("screw rotations from arbitrary 0. Larger means less firm screw.")
    plt.ylabel("stepsize [nm]")
    
    data_2 = np.genfromtxt("C:/MaskAligner/Data/CalibrationScrew/31_10_23.csv", skip_header=1, delimiter=",")
    data_2 = data_2.T
    x = data_2[0]
    y_up = data_2[1] / factor * 10**6
    y_down = data_2[2] / factor * 10**6
    y_err = 0.04 / factor * 10**6
    y_err_2 = 0.1 / factor * 10**6
    
    data_3 = np.genfromtxt("C:/MaskAligner/Data/CalibrationScrew/NewPlate_03_11_23.csv", skip_header=1, delimiter=",")
    print(data_3)
    data_3 = data_3.T
    x_2 = data_3[0]
    y_up_2 = data_3[1] / factor * 10**6
    y_down_2 = data_3[2] / factor * 10**6
    y_err_2 = data_3[3] / factor * 10**6
    y_err_2_2 = data_3[4] / factor * 10**6
    
    plt.errorbar(x, y_up, z_err, label="z3+ with new plate", marker=".", capsize=3)
    plt.errorbar(x, y_down, z_err, label="z3- with new plate", marker=".", capsize=3)
    
    plt.legend()
    plt.savefig("Plots/Z3Examine/ScrewRot_CompareSwappedPlate.png", dpi=300)
    plt.savefig("Plots/Z3Examine/ScrewRot_CompareSwappedPlate.pdf", dpi=300)
    plt.show()
    
    #plt.errorbar(-rot_dp + 5, z3_dp_p, z_err, label="z3+ with z1 front plate", marker=".", capsize=3)
    #plt.errorbar(-rot_dp + 5, z3_dp_m, z_err, label="z3- with z1 front plate", marker=".", capsize=3)
    plt.xlabel("screw rotations from arbitrary 0. Larger means less firm screw.")
    plt.ylabel("stepsize [nm]")
    
    plt.errorbar(x_2, y_up_2, y_err_2, label="z3+ with new plate 02_11", marker=".", capsize=3)
    plt.errorbar(x_2, y_down_2, y_err_2_2, label="z3- with new plate 02_11", marker=".", capsize=3)
    #plt.errorbar(x, y_up, y_err, label="z3+ with new plate", marker=".", capsize=3)
    #plt.errorbar(x, y_down, y_err, label="z3- with new plate", marker=".", capsize=3)
    plt.errorbar(rot, z2p, z_err, label="z2+", marker=".", capsize=3)
    plt.errorbar(rot, z2m, z_err, label="z2-", marker=".", capsize=3)
    
    plt.legend()
    plt.savefig("Plots/Z3Examine/ScrewRot_SwappedPlate.png", dpi=300)
    plt.savefig("Plots/Z3Examine/ScrewRot_SwappedPlate.pdf", dpi=300)
    plt.show()


if __name__ == "__main__":
    main()