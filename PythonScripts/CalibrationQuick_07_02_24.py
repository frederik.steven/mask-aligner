# -*- coding: utf-8 -*-
"""
Created on Wed Feb  7 14:40:15 2024

@author: admin
"""

import matplotlib.pyplot as plt
import numpy as np

def main():
    file = "C:\MaskAligner\Data\Calibration07_02_24\CalibrationQuick.csv"
    data = np.genfromtxt(file, delimiter=",", skip_header=1).T
    
    Z1m = data[1]
    Z1p = data[2]
    
    Z2m = data[4]
    Z2p = data[5]

    Z3m = data[7]
    Z3p = data[8]
    
    
    plt.plot(np.zeros(len(Z1m)) + 0.1, Z1m, color="tab:blue", label="Z1", marker=".", linestyle="none")
    plt.plot(np.zeros(len(Z2m)), Z2m, color="tab:orange", label="Z2", marker=".", linestyle="none")
    plt.plot(np.zeros(len(Z3m)) - 0.1, Z3m, color="tab:green", label="Z3", marker=".", linestyle="none")
    
    plt.plot(np.ones(len(Z1p)) + 0.1, Z1p, color="tab:blue", marker=".", linestyle="none")
    plt.plot(np.ones(len(Z2p)), Z2p, color="tab:orange", marker=".", linestyle="none")
    plt.plot(np.ones(len(Z3p)) - 0.1, Z3p, color="tab:green", marker=".", linestyle="none")
    
    plt.errorbar(0 + 0.1, np.nanmean(Z1m), np.nanstd(Z1m, ddof=1), color="tab:blue", capsize=3, marker="_")
    plt.errorbar(1 + 0.1, np.nanmean(Z1p), np.nanstd(Z1p, ddof=1), color="tab:blue", capsize=3, marker="_")
    
    plt.errorbar(0, np.nanmean(Z2m), np.nanstd(Z2m, ddof=1), color="tab:orange", capsize=3, marker="_")
    plt.errorbar(1, np.nanmean(Z2p), np.nanstd(Z2p, ddof=1), color="tab:orange", capsize=3, marker="_")
    
    plt.errorbar(0 - 0.1, np.nanmean(Z3m), np.nanstd(Z3m, ddof=1), color="tab:green", capsize=3, marker="_")
    plt.errorbar(1 - 0.1, np.nanmean(Z3p), np.nanstd(Z3p, ddof=1), color="tab:green", capsize=3, marker="_")
    
    plt.xticks([0,1], ["-", "+"])
    plt.xlabel("direction")
    plt.ylabel(r"time [s] for distance of $\approx 2$ mm")
    
    plt.legend()
    plt.show()

if __name__ == "__main__":
    main()