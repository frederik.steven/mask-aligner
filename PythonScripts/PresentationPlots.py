# -*- coding: utf-8 -*-
"""
Created on Tue Jan  9 10:39:27 2024

@author: admin
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy as sp

def sin_s(t):
    x = np.where(t < np.pi, 4095 * (-1.0/(2.0*np.pi) * np.sin(2*np.pi * t / np.pi) + t / np.pi), 4095)
    return x

t = np.arange(0, np.pi, 0.001)
pulse = sin_s(t)

factor = 1/4095 * 3.3 

#Signal Generated
fig, ax = plt.subplots()
#plt.plot(t, pulse)
ax.plot(t / np.pi, pulse, linestyle="none", marker=".", markersize=1, label="DAC0")
ax.plot(t / np.pi, 4095 - pulse, linestyle="none", marker=".", markersize=1, label="DAC1")
plt.legend()
ax.set_xlabel(r"$\frac{t}{T}$")
ax.set_ylabel("Amplitude in arb. units")
fig.savefig("Plots/Presentation/GeneratingSignal.pdf")
fig.savefig("Plots/Presentation/GeneratingSignal.png", dpi=300)
plt.show()

#Signal Generated
fig, ax = plt.subplots()
#plt.plot(t, pulse)
#ax.plot(t, pulse, linestyle="none", marker=".", markersize=1, label="DAC0")
ax.plot(t / np.pi, pulse - (4095 - pulse), linestyle="none", marker=".", markersize=1, label="DAC0")
ax.plot(t / np.pi, (4095 - pulse) - pulse, linestyle="none", marker=".", markersize=1, label="DAC1")
plt.legend()
ax.set_xlabel(r"$\frac{t}{T}$")
ax.set_ylabel("Amplitude in arb. units")
fig.savefig("Plots/Presentation/GeneratingSignal.pdf")
fig.savefig("Plots/Presentation/GeneratingSignal.png", dpi=300)
plt.show()

#Signal Generated
fig, ax = plt.subplots()
#plt.plot(t, pulse)
ax.plot(t / np.pi, pulse.astype(np.int16), linestyle="none", marker=".", markersize=1, label="DAC0")
ax.plot(t / np.pi, 4095 - pulse.astype(np.int16), linestyle="none", marker=".", markersize=1, label="DAC1")
plt.legend()
ax.set_xlabel(r"$\frac{t}{T}$")
ax.set_ylabel("Amplitude in arb. units")
fig.savefig("Plots/Presentation/GeneratingSignalInt.pdf")
fig.savefig("Plots/Presentation/GeneratingSignalInt.png", dpi=300)
plt.show()

#Signal In Dac At First
fig2, ax2 = plt.subplots()
b, a = sp.signal.bessel(8, 0.05, btype='low', analog=False)
noised_signal_0 = pulse.astype(np.int16) + np.random.normal(0, 50, len(pulse))
noised_signal_1 = 4095 - pulse.astype(np.int16) + np.random.normal(0, 50, len(pulse))
ax2.plot(t / np.pi, factor * noised_signal_0, marker=".", markersize=1, label="DAC0 noisy")
#ax2.plot(t, factor * filtered_pulse, marker=".", markersize=1, label="DAC0 bessel filtered")
ax2.plot(t / np.pi, factor * noised_signal_1, marker=".", markersize=1, label="DAC1 noisy")
#ax2.plot(t, factor * (4095 - filtered_pulse), marker=".", markersize=1, label="DAC1 bessel filtered")
plt.legend()
ax2.set_xlabel(r"$\frac{t}{T}$")
ax2.set_ylabel("U [V]")
fig2.savefig("Plots/Presentation/DACSTART.pdf")
fig2.savefig("Plots/Presentation/DACSTART.png", dpi=300)
plt.show()

pulse, a_pulse = pulse - (4095 - pulse), (4095 - pulse) - pulse

#Bessel Filter
fig2, ax2 = plt.subplots()
b, a = sp.signal.bessel(8, 0.2, btype='low', analog=False)
noised_signal_0 = pulse.astype(np.int16) + np.random.normal(0, 50, len(pulse))
noised_signal_1 = a_pulse.astype(np.int16) + np.random.normal(0, 50, len(pulse))

filtered_pulse_0 = sp.signal.filtfilt(b, a, noised_signal_0)
filtered_pulse_1 = sp.signal.filtfilt(b, a, noised_signal_1)

ax2.plot(t / np.pi, factor * noised_signal_0, marker=".", markersize=1, label="Signal noisy")
#ax2.plot(t, factor * filtered_pulse, marker=".", markersize=1, label="DAC0 bessel filtered")
#ax2.plot(t, factor * noised_signal_1, marker=".", markersize=1, label="DAC1 noisy")
#ax2.plot(t, factor * (4095 - filtered_pulse), marker=".", markersize=1, label="DAC1 bessel filtered")
plt.legend()
ax2.set_xlabel(r"$\frac{t}{T}$")
ax2.set_ylabel("U [V]")
fig2.savefig("Plots/Presentation/Filtering.pdf")
fig2.savefig("Plots/Presentation/Filtering.png", dpi=300)
plt.show()

fig2, ax2 = plt.subplots()
#ax2.plot(t, factor * noised_signal, marker=".", markersize=1, label="DAC0 noisy")
ax2.plot(t / np.pi, factor * filtered_pulse_0, marker=".", markersize=1, label="Signal bessel filtered")
#ax2.plot(t, factor * (4095 - noised_signal), marker=".", markersize=1, label="DAC1 noisy")
#ax2.plot(t, factor * filtered_pulse_1, marker=".", markersize=1, label="DAC1 bessel filtered")
plt.legend()
ax2.set_xlabel(r"$\frac{t}{T}$")
ax2.set_ylabel("U [V]")
fig2.savefig("Plots/Presentation/FilteringDone.pdf")
fig2.savefig("Plots/Presentation/FilteringDone.png", dpi=300)
plt.show()


#Generating Inverted Signal
fig3, ax3 = plt.subplots()
ax3.plot(t / np.pi, filtered_pulse_0 * factor, marker=".", markersize=1, label="Signal")
ax3.plot(t / np.pi, -filtered_pulse_0 * factor, marker=".", markersize=1, label="Signal inverted")
plt.legend()
ax3.set_xlabel(r"$\frac{t}{T}$")
ax3.set_ylabel("U [V]")
fig3.savefig("Plots/Presentation/InvertedSginal.pdf")
fig3.savefig("Plots/Presentation/InvertedSginal.png", dpi=300)
plt.show()

#Switching between Inverted and not inverted Signal
fig4, ax4 = plt.subplots()
ax4.plot(np.concatenate((t,t + np.pi))[: len(t)-1] / (2 * np.pi) , np.concatenate((filtered_pulse_0, -filtered_pulse_1))[: len(t)-1] * factor, marker=".", markersize=1, label="Signal")
ax4.plot(np.concatenate((t,t + np.pi))[len(t):] / (2 * np.pi) , np.concatenate((filtered_pulse_0, -filtered_pulse_1))[len(t):] * factor, marker=".", markersize=1, label="Signal inverted")
ax4.plot(np.concatenate((t,t + np.pi))[len(t)-1 : len(t)+1] / (2 * np.pi)  , np.concatenate((filtered_pulse_0, -filtered_pulse_1))[len(t)-1  : len(t)+1] * factor, marker=".", markersize=1, label="switch")
plt.legend()
ax4.set_xlabel(r"$\frac{t}{T}$")
ax4.set_ylabel("U [V]")
fig4.savefig("Plots/Presentation/Switching01.pdf")
fig4.savefig("Plots/Presentation/Switching01.png", dpi=300)
plt.show()

#Switching between Inverted and not inverted Signal
fig5, ax5 = plt.subplots()
ax5.plot(np.concatenate((t,t + np.pi))[: len(t)-1] / (2 * np.pi) , np.concatenate((filtered_pulse_1, -filtered_pulse_0))[: len(t)-1] * factor, marker=".", markersize=1, label="Signal inverted")
ax5.plot(np.concatenate((t,t + np.pi))[len(t)-1 : len(t)+1] / (2 * np.pi)  , np.concatenate((filtered_pulse_1, -filtered_pulse_0))[len(t)-1  : len(t)+1] * factor, marker=".", markersize=1, label="switch")
ax5.plot(np.concatenate((t,t + np.pi))[len(t):] / (2 * np.pi) , np.concatenate((filtered_pulse_1, -filtered_pulse_0))[len(t):] * factor, marker=".", markersize=1, label="Signal")
plt.legend()
ax5.set_xlabel(r"$\frac{t}{T}$")
ax5.set_ylabel("U [V]")
fig5.savefig("Plots/Presentation/Switching02.pdf")
fig5.savefig("Plots/Presentation/Switching02.png", dpi=300)
plt.show()

#Switching between Inverted and not inverted Signal
fig6, ax6 = plt.subplots()
ax6.plot(np.concatenate((t,t + np.pi))[: len(t)-1] / (2 * np.pi) , np.concatenate((filtered_pulse_0, -filtered_pulse_1))[: len(t)-1] * factor * 24.24, marker=".", markersize=1, label="Signal")
ax6.plot(np.concatenate((t,t + np.pi))[len(t):] / (2 * np.pi) , np.concatenate((filtered_pulse_0, -filtered_pulse_1))[len(t):] * factor* 24.24, marker=".", markersize=1, label="Signal inverted")
ax6.plot(np.concatenate((t,t + np.pi))[len(t)-1 : len(t)+1] / (2 * np.pi)  , np.concatenate((filtered_pulse_0, -filtered_pulse_1))[len(t)-1  : len(t)+1] * factor* 24.24, marker=".", markersize=1, label="switch")
plt.legend()
ax6.set_xlabel(r"$\frac{t}{T}$")
ax6.set_ylabel("U [V]")
fig6.savefig("Plots/Presentation/FinalAmplification.pdf")
fig6.savefig("Plots/Presentation/FinalAmplification.png", dpi=300)
plt.show()
